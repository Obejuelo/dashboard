module.exports = {
  typescript: {
    ignoreBuildErrors: true
  },
  i18n: {
    locales: ['en-US', 'es'],
    defaultLocale: 'en-US'
  },
  compiler: {
    styledComponents: true
  }
};
