import '../../chart-js-defaults';
import React from 'react';
import { SnackbarProvider } from 'notistack';
import App, { AppContext, AppInitialProps, AppProps } from 'next/app';

import ThemeLayout from '../layouts/theme/theme';
import { wrapper } from '../store/create-store';
import { GlobalContextProvider } from 'context/global';

class MyApp extends App<AppInitialProps> {
  componentDidMount() {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentNode?.removeChild(jssStyles);
    }
  }

  public static getInitialProps = async ({ Component, ctx }: AppContext) => {
    return {
      pageProps: {
        ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}),
        appProps: ctx.pathname
      }
    };
  };

  render() {
    const { Component, pageProps }: AppProps = this.props;

    return (
      <GlobalContextProvider>
        <ThemeLayout>
          <SnackbarProvider>
            <Component {...pageProps} />
          </SnackbarProvider>
        </ThemeLayout>
      </GlobalContextProvider>
    );
  }
}

export default wrapper.withRedux(MyApp);
