import React, { useEffect } from 'react';
import { Box } from '@mui/material';
import { useDispatch } from 'react-redux';

import { getChatUserData, getContacts } from 'store/chat/chat.action';
import { DashboardLayout } from 'layouts/dashboard';
import { SEO } from 'components/common/seo';
import { Chat } from 'components/chat';

const ChatPage: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getContacts());
    dispatch(getChatUserData());
  }, []);

  return (
    <DashboardLayout>
      <SEO title="Chat" />
      <Box
        height={1}
        display="flex"
        justifyContent="center"
        p={{ xs: 1, md: 2 }}
      >
        <Chat />
      </Box>
    </DashboardLayout>
  );
};

export default ChatPage;
