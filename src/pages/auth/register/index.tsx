import React from 'react';
import { Button, Link, Paper, Theme, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';
import { Box } from '@mui/system';
import { useFormik } from 'formik';
import Router from 'next/router';

import { AuthLayout } from 'layouts/auth';
import { registerWithEmail } from 'store/auth/auth.action';
import { Input } from 'components/common/form/input';
import FormBody from 'components/common/form/body';
import { SEO } from 'components/common/seo';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    padding: '2rem 0.5rem',
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      padding: '2rem 1rem',
      width: 400
    }
  }
}));

const RegisterPage: React.FC = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [isLoading, setIsLoading] = React.useState(false);

  const handleSubmit = async (values: any) => {
    setIsLoading(true);

    await dispatch(
      registerWithEmail({
        name: values.name,
        email: values.name,
        password: values.password
      })
    );
    setIsLoading(false);
  };

  const formik = useFormik({
    initialValues: { name: '', password: '', email: '' },
    onSubmit: handleSubmit
  });

  return (
    <AuthLayout>
      <SEO title="Login" />
      <Box
        px={{ xs: 1, sm: 2 }}
        width={1}
        display="flex"
        justifyContent="center"
      >
        <Paper className={classes.paper}>
          <Box mb={4}>
            <Typography align="center" variant="h3">
              Register
            </Typography>
          </Box>
          <form onSubmit={formik.handleSubmit}>
            <FormBody isLoading={isLoading}>
              <Box>
                <Input
                  label="User name"
                  fullWidth
                  name="name"
                  formik={formik}
                  size="medium"
                />
              </Box>
              <Box my={2}>
                <Input
                  label="Email"
                  name="email"
                  fullWidth
                  formik={formik}
                  type="email"
                  size="medium"
                />
              </Box>
              <Box my={2}>
                <Input
                  label="Password"
                  name="password"
                  fullWidth
                  formik={formik}
                  type="password"
                  size="medium"
                />
              </Box>
            </FormBody>
            <Button
              fullWidth
              type="submit"
              variant="contained"
              disabled={isLoading}
              size="large"
            >
              Continue
            </Button>
          </form>
          <Box display="flex" alignItems="center" flexDirection="column" mt={2}>
            <Typography variant="body2">Or</Typography>
            <Link component="button" onClick={() => Router.push('/auth/login')}>
              Login
            </Link>
          </Box>
        </Paper>
      </Box>
    </AuthLayout>
  );
};

export default RegisterPage;
