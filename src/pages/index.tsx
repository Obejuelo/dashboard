import React from 'react';

import { useRouteGuard } from 'hooks';
import { SplashScreen } from 'components/common/splash-screen/splash-screen';

const MyApp: React.FC = () => {
  useRouteGuard('INDEX');

  return <SplashScreen />;
};

export default MyApp;
