import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Grid } from '@mui/material';

import { DashboardLayout } from 'layouts/dashboard';
import { getAnalyticWidgets } from 'store/analytics/analytics.action';
import { AppState } from 'store/reducer';
import UsersDevice from 'components/analytics/widgets/users-device';
import AnalyticSales from 'components/analytics/widgets/sales';
import AnalyticCampaigns from 'components/analytics/widgets/campaigns';
import AnalyticConversion from 'components/analytics/widgets/conversion';
import AnalyticImpressions from 'components/analytics/widgets/impressions';
import AnalyticVisitors from 'components/analytics/widgets/visitors';
import AnalyticPageViews from 'components/analytics/widgets/page-views';
import { SEO } from 'components/common/seo';

const AnalyticsPage: React.FC = () => {
  const dispatch = useDispatch();
  const analytics = useSelector((state: AppState) => state.analytics.data);

  useEffect(() => {
    const getAnalyticData = async () => {
      dispatch(getAnalyticWidgets());
    };
    getAnalyticData();
  }, []);

  return (
    <DashboardLayout>
      <SEO title="Analytics" />
      <Box p={{ xs: 1, md: 2 }}>
        <Grid container spacing={2} alignItems="stretch" direction="row">
          <Grid item container xs={12} lg={9}>
            <Grid item xs={12}>
              {analytics && <AnalyticPageViews data={analytics.widget5} />}
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} lg={4}>
                {analytics && <AnalyticVisitors data={analytics.widget4} />}
              </Grid>
              <Grid item xs={12} lg={4}>
                {analytics && <AnalyticImpressions data={analytics.widget3} />}
              </Grid>
              <Grid item xs={12} lg={4}>
                {analytics && <AnalyticConversion data={analytics.widget2} />}
              </Grid>
            </Grid>
            <Grid item xs={12}>
              {analytics && <AnalyticPageViews data={analytics.widget5} />}
            </Grid>
          </Grid>
          <Grid item container xs={12} lg={3} spacing={2}>
            <Grid item xs={12}>
              {analytics && <UsersDevice data={analytics.widget7} />}
            </Grid>
            <Grid item xs={12}>
              {analytics && <AnalyticSales data={analytics.widget8} />}
            </Grid>
            <Grid item xs={12}>
              {analytics && <AnalyticCampaigns data={analytics.widget9} />}
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </DashboardLayout>
  );
};

export default AnalyticsPage;
