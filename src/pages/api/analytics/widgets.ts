import { NextApiRequest, NextApiResponse } from 'next';
import { analyticsDB } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  res.status(200).json(analyticsDB.widgets);
};
