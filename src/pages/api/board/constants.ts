export const boardDB = {
  boards: [
    {
      id: '27cfcbe1',
      name: 'Test board application',
      uri: 'test-board-application',
      lists: [
        {
          id: '56027cf5a2ca3839a5d36103',
          name: 'TODO',
          idCards: [
            '5603a2a3cab0c8300f6096b3',
            '5637273da9b93bb84743a0f9',
            '5637273da9b93bb84743a0f0'
          ]
        },
        {
          id: '56027cf5a2ca3839a5d36104',
          name: 'In Progress',
          idCards: ['5603a2a3cab0c8300f6096b4']
        },
        {
          id: '57027cf5a2ca3839a5d36104',
          name: 'Review',
          idCards: ['5637283da9b93bb84743a0f0', '5637293da9b93bb84743a0a0']
        }
      ],
      cards: [
        {
          id: '5603a2a3cab0c8300f6096b3',
          name: 'Calendar App Design',
          description:
            'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system.'
        },
        {
          id: '5637273da9b93bb84743a0f9',
          name: 'Fix Splash Screen bugs',
          description:
            'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.'
        },
        {
          id: '5603a2a3cab0c8300f6096b4',
          name: 'Segment Track',
          description:
            'Denouncing pleasure and praising pain was born and I will give you a complete account of the system.'
        },
        {
          id: '5637273da9b93bb84743a0f0',
          name: 'Responsive Design',
          description:
            'Denouncing pleasure and praising pain was born and I will give you a complete account of the system, pleasure and praising pain'
        },
        {
          id: '5637283da9b93bb84743a0f0',
          name: 'Fixing Navbar',
          description: 'Fixing navbar with user menu and dark theme'
        },
        {
          id: '5637293da9b93bb84743a0a0',
          name: 'Sentry Implement',
          description: 'Implement sentry for errors tracking'
        }
      ]
    }
  ]
};
