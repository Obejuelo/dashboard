import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';

import { boardDB } from '../constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { boardId, data } = req.body;
  const board: any = _.find(boardDB.boards, { id: boardId });
  const lists = [...board.lists, data];
  _.assign(board, { lists });

  res.status(200).json(lists);
};
