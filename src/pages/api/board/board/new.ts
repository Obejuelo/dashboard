import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';
import { boardDB } from '../constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { board } = req.body;
  boardDB.boards = [...boardDB.boards, board];
  res.status(200).json(_.pick(board, ['id', 'name', 'uri']));
};
