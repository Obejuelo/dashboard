import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';

import { boardDB } from '../constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { boardId, boardTitle } = req.body;
  const board = _.find(boardDB.boards, { id: boardId });
  _.set(board, 'name', boardTitle);

  res.status(200).json(boardTitle);
};
