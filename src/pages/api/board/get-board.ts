import { NextApiRequest, NextApiResponse } from 'next';

import { boardDB } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  res.status(200).json(boardDB.boards[0]);
};
