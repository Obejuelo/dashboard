import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';
import { boardDB } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const response = _.map(boardDB.boards, (board: any) =>
    _.pick(board, ['id', 'name', 'uri'])
  );

  res.status(200).json(response);
};
