import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';

import { boardDB } from '../constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { boardId, card } = req.body;
  const board: any = _.find(boardDB.boards, { id: boardId });
  const selectedCard = _.find(board.cards, { id: card.id });
  _.assign(selectedCard, card);

  res.status(200).json(card);
};
