import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';

import { boardDB } from '../constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { boardId, lists } = req.body;
  const board = _.find(boardDB.boards, { id: boardId });
  _.assign(board, { lists });
  res.status(200).json(lists);
};
