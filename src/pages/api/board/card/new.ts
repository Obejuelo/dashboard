import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';

import { boardDB } from '../constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { boardId, listId, data } = req.body;
  const board: any = _.find(boardDB.boards, { id: boardId }) || {};

  _.assign(board, {
    cards: [...board.cards, data],
    lists: _.map(board.lists, (_list: any) => {
      if (_list.id === listId) {
        _.assign(_list, { idCards: [..._list.idCards, data.id] });
      }
      return _list;
    })
  });

  res.status(200).json(board);
};
