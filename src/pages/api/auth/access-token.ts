import { NextApiRequest, NextApiResponse } from 'next';
import jwt from 'jsonwebtoken';
import _ from 'lodash';

import { authDB, jwtConfig } from './constants';

export default async (rep: NextApiRequest, res: NextApiResponse) => {
  const { access_token } = rep.body;

  try {
    const { id } = jwt.verify(access_token, jwtConfig.secret) as any;
    const user = _.cloneDeep(authDB.users.find(_user => _user.uuid === id));
    const updatedAccessToken = jwt.sign({ id: user?.uuid }, jwtConfig.secret, {
      expiresIn: jwtConfig.expiresIn
    });

    const response = {
      user: user,
      access_token: updatedAccessToken
    };

    res.status(200).json(response);
  } catch (e) {
    const error = 'Invalid access token detected';
    res.status(401).json(error);
  }
};
