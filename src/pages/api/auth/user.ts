import { NextApiRequest, NextApiResponse } from 'next';

import { authDB } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { uuid } = req.body;

  const userData = authDB.users.find(user => user.uuid === uuid);
  if (userData) {
    res.status(200).json(userData.data);
  } else {
    res.status(200).json('User Not Found');
  }
};
