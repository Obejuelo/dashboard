import { NextApiRequest, NextApiResponse } from 'next';
import jwt from 'jsonwebtoken';
import _ from 'lodash';

import { authDB, jwtConfig } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { email, password } = req.body;
  const errorMessage = 'Check your email or password';

  const user = _.cloneDeep(
    authDB.users.find(_user => _user.data.email === email)
  );

  const error = {
    email: user ? null : errorMessage,
    password: user && user.password === password ? null : errorMessage
  };

  if (!error.email && !error.password) {
    const access_token = jwt.sign({ id: user?.uuid }, jwtConfig.secret, {
      expiresIn: jwtConfig.expiresIn
    });

    const response = {
      user: user,
      access_token: access_token
    };
    res.status(200).json(response);
  } else {
    res.status(401).json(error);
  }
};
