import { NextApiRequest, NextApiResponse } from 'next';
import jwt from 'jsonwebtoken';
import _ from 'lodash';

import { generateGUID } from 'utils/generate-GUID';
import { authDB, jwtConfig } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { name, password, email } = req.body;
  const isEmailExists = authDB.users.find(_user => _user.data.email === email);

  const error = {
    email: isEmailExists ? 'The email is already in use' : null,
    displayName: name !== '' ? null : 'Enter display name',
    password: null
  };

  if (!error.displayName && !error.password && !error.email) {
    const newUser = {
      uuid: generateGUID(),
      from: 'custom-db',
      password: password,
      role: 'admin',
      data: {
        displayName: name,
        email: email,
        theme: 'light'
      }
    };

    authDB.users = [...authDB.users, newUser];

    const user = _.cloneDeep(newUser);
    delete user['password'];

    const access_token = jwt.sign({ id: user.uuid }, jwtConfig.secret, {
      expiresIn: jwtConfig.expiresIn
    });

    const response = {
      user: user,
      access_token: access_token
    };

    res.status(200).json(response);
  } else {
    res.status(200).json(error);
  }
};
