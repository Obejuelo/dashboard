import { NextApiRequest, NextApiResponse } from 'next';
import { chatDb } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  res.status(200).json(chatDb.contacts);
};
