import { NextApiRequest, NextApiResponse } from 'next';
import { chatDb } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const data = req.body;
  const { chatId, message, contactId } = data;
  const chat = chatDb.chats.find(_chat => _chat.id === chatId);

  if (chat) {
    chat.dialog = [...chat.dialog, message];
  }

  chatDb.user[0].chatList.find(
    _contact => _contact.id === contactId
  ).lastMessageTime = message.time;

  res.status(200).json({
    message,
    userChatData: chatDb.user[0].chatList.find(
      _contact => _contact.chatId === contactId
    )
  });
};
