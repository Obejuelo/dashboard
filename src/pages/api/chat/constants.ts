import { generateGUID } from 'utils/generate-GUID';
import { ChatAppType, ChatAppUserType } from './types';

export const createNewChat = (contactId: string, userId: string) => {
  const chatId = generateGUID();
  const user: ChatAppUserType | undefined = chatDb.user.find(
    _user => _user.id === userId
  );

  if (user) {
    user.chatList = [
      {
        chatId: chatId,
        contactId: contactId,
        lastMessageTime: ''
      },
      ...chatDb.user[0].chatList
    ];
  }
  chatDb.chats = [
    ...chatDb.chats,
    {
      id: chatId,
      dialog: []
    }
  ];
  return chatId;
};

export const chatDb: ChatAppType = {
  contacts: [
    {
      id: '5725a680b3249760ea21de53',
      name: 'Aaliyah Love',
      status: 'online',
      mood: 'I am using chat app',
      unread: '4'
    },
    {
      id: '5725a680b3249760ea21de52',
      name: 'Mattie Robertson',
      status: 'online',
      mood: 'I am using chat app',
      unread: '4'
    },
    {
      id: '5725a680606588342058356d',
      name: 'Damian Payne',
      status: 'offline',
      mood: 'I am using chat app',
      unread: '5'
    },
    {
      id: '5725a68009e20d0a9e9acf2a',
      name: 'Mallory Joseph',
      status: 'online',
      mood: 'I am using chat app',
      unread: '2'
    },
    {
      id: '5725a6809fdd915739187ed5',
      name: 'Sarah Berry',
      status: 'offline',
      mood: 'I am using chat app'
    },
    {
      id: '5725a68007920cf75051da64',
      name: 'Jay Richards',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a68031fdbb1db2c1af47',
      name: 'Jazmin Lara',
      status: 'offline',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680bc670af746c435e2',
      name: 'Makenna Booker',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680e7eb988a58ddf303',
      name: 'Brittany Oconnell',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680dcb077889f758961',
      name: 'Miguel Zuniga',
      status: 'offline',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6806acf030f9341e925',
      name: 'Luciano Stafford',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680ae1ae9a3c960d487',
      name: 'Henderson',
      status: 'offline',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680b8d240c011dd224b',
      name: 'Cameron Glass',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a68034cb3968e1f79eac',
      name: 'Kelvin Forbes',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6801146cce777df2a08',
      name: 'Dana',
      status: 'offline',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6808a178bfd034d6ecf',
      name: 'Cameron',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680653c265f5c79b5a9',
      name: 'London Holt',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680bbcec3cc32a8488a',
      name: 'Raymond Pineda',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6803d87f1b77e17b62b',
      name: 'Lila Coleman',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680e87cb319bd9bd673',
      name: 'Reina Burton',
      status: 'offline',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6802d10e277a0f35775',
      name: 'Marques Coffey',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680aef1e5cf26dd3d1f',
      name: 'Micah Cox',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a680cd7efa56a45aea5d',
      name: 'Brooks Donovan',
      status: 'online',
      mood: ''
    },
    {
      id: '5725a680fb65c91a82cb35e2',
      name: 'Trevino',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a68018c663044be49cbf',
      name: 'Mateo',
      status: 'offline',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6809413bf8a0a5272b1',
      name: 'Leonardo',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6809413bf8a0a5272b2',
      name: 'Braylen Adams',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6809413bf8a0a5272b3',
      name: 'Corbin Hartman',
      status: 'online',
      mood: 'I am using chat app'
    },
    {
      id: '5725a6809413bf8a0a5272b4',
      name: 'Akira',
      status: 'online',
      mood: 'I am using chat app'
    }
  ],
  chats: [
    {
      id: '1725a680b3249760ea21de52',
      dialog: [
        {
          who: '5725a680b3249760ea21de52',
          message: 'How do you two know each other?',
          time: '2022-01-22T08:54:28.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message: 'So, what do you do for a living?',
          time: '2022-01-22T08:55:28.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message: 'How long have you been doing that',
          time: '2022-01-22T08:56:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'We are losing money! Quick!',
          time: '2022-01-22T09:00:28.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message:
            'Hi, I’m John. Yeah, it’s been great! Are you enjoying it too?',
          time: '2022-01-22T09:02:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'Hi, I’m John. It’s nice to meet you too',
          time: '2022-01-22T09:05:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'We are losing money! Quick!',
          time: '2022-01-22T09:14:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'You are the worst!',
          time: '2022-01-22T09:16:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'We are losing money! Quick!',
          time: '2022-01-22T09:17:28.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message:
            'Hi, I’m John. Yeah, it’s been great! Are you enjoying it too',
          time: '2022-01-22T09:20:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'You are the worst!',
          time: '2022-01-22T09:22:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'We are losing money! Quick!',
          time: '2022-01-22T09:25:28.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message:
            'Hi, I’m John. Yeah, it’s been great! Are you enjoying it too',
          time: '2022-01-22T09:27:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'You are the worst!',
          time: '2022-01-22T09:33:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'You are the worst!',
          time: '2022-01-22T09:34:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'We are losing money! Quick!',
          time: '2022-01-22T09:35:28.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message:
            'Hi, I’m John. Yeah, it’s been great! Are you enjoying it too',
          time: '2022-01-22T09:45:28.299Z'
        },
        {
          who: '5725a680b3249760ea21de52',
          message: 'You are the worst!',
          time: '2022-01-22T10:00:28.299Z'
        }
      ]
    },
    {
      id: '2725a680b8d240c011dd2243',
      dialog: [
        {
          who: '5725a680606588342058356d',
          message:
            'Quickly come to the meeting room 1B, we have a big server issue',
          time: '2022-02-01T01:00:00.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message:
            'I’m having breakfast right now, can’t you wait for 10 minutes?',
          time: '2022-02-01T01:05:00.299Z'
        },
        {
          who: '5725a680606588342058356d',
          message: 'We are losing money! Quick!',
          time: '2022-02-01T01:10:00.299Z'
        },
        {
          who: '5725a680606588342058356d',
          message:
            'Quickly come to the meeting room 1B, we have a big server issue',
          time: '2022-02-01T01:15:00.299Z'
        },
        {
          who: '5725a6802d10e277a0f35724',
          message:
            'I’m having breakfast right now, can’t you wait for 10 minutes?',
          time: '2022-02-01T01:05:20.299Z'
        },
        {
          who: '5725a680606588342058356d',
          message: 'We are losing money! Quick!',
          time: '2022-02-01T01:10:21.299Z'
        }
      ]
    },
    {
      id: '3725a6809413bf8a0a5272b4',
      dialog: [
        {
          who: '5725a68009e20d0a9e9acf2a',
          message:
            'Quickly come to the meeting room 1B, we have a big server issue',
          time: '2022-02-01T02:10:00.299Z'
        }
      ]
    }
  ],
  user: [
    {
      id: '5725a6802d10e277a0f35724',
      name: 'John Doe',
      status: 'online',
      mood: "I'm using chat app...",
      chatList: [
        {
          chatId: '1725a680b3249760ea21de52',
          contactId: '5725a680b3249760ea21de52',
          lastMessageTime: '2017-06-12T02:10:18.931Z'
        },
        {
          chatId: '2725a680b8d240c011dd2243',
          contactId: '5725a680606588342058356d',
          lastMessageTime: '2017-02-18T10:30:18.931Z'
        },
        {
          chatId: '3725a6809413bf8a0a5272b4',
          contactId: '5725a68009e20d0a9e9acf2a',
          lastMessageTime: '2017-03-18T12:30:18.931Z'
        }
      ]
    }
  ]
};
