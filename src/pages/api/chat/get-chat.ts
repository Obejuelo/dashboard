/* eslint-disable prefer-const */
import { NextApiRequest, NextApiResponse } from 'next';
import { chatDb, createNewChat } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { contactId, userId } = req.query;

  let response;
  const user = chatDb.user.find(_user => _user.id === userId);
  const chat =
    user && user.chatList.find(_chat => _chat.contactId === contactId);
  const chatId = chat
    ? chat.chatId
    : createNewChat(contactId as string, userId as string);
  response = chatDb.chats.find(_chat => _chat.id === chatId);

  res.status(200).json({
    chat: response,
    userChatData:
      user && user.chatList.find(_chat => _chat.contactId === contactId)
  });
};
