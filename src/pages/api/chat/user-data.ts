import { NextApiRequest, NextApiResponse } from 'next';
import _ from 'lodash';

import { chatDb } from './constants';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const data = req.query;
  chatDb.user[0] = _.merge({}, chatDb.user[0], data);
  res.status(200).json(chatDb.user[0]);
};
