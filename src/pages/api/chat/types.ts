export type ChatAppType = {
  contacts: ChatAppContactsType[];
  chats: ChatAppChatType[];
  user: ChatAppUserType[];
};

export type ChatAppContactsType = {
  id: string;
  name: string;
  avatar?: string;
  status: string;
  mood: string;
  unread?: string;
};

export type ChatAppChatType = {
  id: string;
  dialog: ChatAppChatDialogType[];
};

export type ChatAppChatDialogType = {
  who: string;
  message: string;
  time: string;
};

export type ChatAppUserType = {
  id: string;
  name: string;
  status: string;
  mood: string;
  chatList: ChatAppUserChatListType[];
};

export type ChatAppUserChatListType = {
  chatId: string;
  contactId: string;
  lastMessageTime: string;
};
