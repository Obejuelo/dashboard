import React from 'react';
import { Typography } from '@mui/material';

import { DashboardLayout } from 'layouts/dashboard';

const HomePage: React.FC = () => {
  return (
    <DashboardLayout>
      <Typography>Home page</Typography>
    </DashboardLayout>
  );
};

export default HomePage;
