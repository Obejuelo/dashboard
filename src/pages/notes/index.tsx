import React from 'react';

import { DashboardLayout } from 'layouts/dashboard';
import { Typography } from '@mui/material';

const ToDoPage: React.FC = () => {
  return (
    <DashboardLayout>
      <Typography>Notes page</Typography>
    </DashboardLayout>
  );
};

export default ToDoPage;
