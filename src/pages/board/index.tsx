import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@mui/material';

import { DashboardLayout } from 'layouts/dashboard';
import { getBoard } from 'store/board/board.actions';
import { AppState } from '../../store/reducer';
import { SEO } from 'components/common/seo';
import Board from 'components/board';

const BoardPage: React.FC = () => {
  const dispatch = useDispatch();
  const board = useSelector((state: AppState) => state?.board?.board);

  useEffect(() => {
    const getBoardList = async () => {
      dispatch(getBoard());
    };
    getBoardList();
  }, []);

  return (
    <DashboardLayout>
      <SEO title="Board" />
      <Box p={{ xs: 1, md: 2 }}>
        <Board data={board.lists} />
      </Box>
    </DashboardLayout>
  );
};

export default BoardPage;
