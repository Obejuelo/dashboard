import 'reflect-metadata';
import React, { createContext, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import useSWR from 'swr';

import { AuthService } from 'services/auth/auth.service';
import { setUserAuthentication, setUserData } from 'store/user/user.action';

type GlobalContextProviderProps = {
  children: React.ReactElement;
};

const GlobalContext = createContext<any>({});

export const GlobalContextProvider: React.FC<GlobalContextProviderProps> = ({
  children
}) => {
  const dispatch = useDispatch();
  const [globalError, setGlobalError] = useState<string>('');

  const validateSession = (): boolean | undefined => {
    console.log('Validating session...');
    const authService = new AuthService();
    const isLogged = authService.signInWithToken();

    if (!isLogged) {
      dispatch(setUserAuthentication(false));
      return false;
    } else {
      getUserData();
      dispatch(setUserAuthentication(true));
    }
  };

  const getUserData = async () => {
    const authService = new AuthService();
    const userData: any = await authService.getUserData();
    dispatch(setUserData(userData.data));
  };

  const { data: isValidSession } = useSWR('validSession', validateSession, {
    revalidateOnFocus: false
  });

  const store = useMemo(() => {
    return {
      isValidSession,
      error: {
        message: globalError,
        setMessage: setGlobalError
      },
      session: null,
      user: null
    };
  }, [isValidSession, globalError]);

  return (
    <GlobalContext.Provider value={{ ...store }}>
      {children}
    </GlobalContext.Provider>
  );
};
