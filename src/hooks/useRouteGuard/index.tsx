import { useEffect } from 'react';
import { mutate } from 'swr';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';

import { AppState } from 'store/reducer';
import { AuthService } from 'services/auth/auth.service';

const useRouteGuard = (
  typeOfRoute: 'PRIVATE' | 'PUBLIC' | 'INDEX'
): boolean | undefined => {
  const router = useRouter();
  const isLogged = useSelector((state: AppState) => state.user.isLogged);

  useEffect(() => {
    const authService = new AuthService();
    const isValidSession = authService.signInWithToken();

    (async () => {
      switch (typeOfRoute) {
        case 'PRIVATE':
          if (isValidSession) {
            mutate('validSession', true, false);
          } else {
            router.push('/auth/login');
          }
          break;

        case 'PUBLIC':
          if (isValidSession) {
            router.push('/analytics');
          }
          mutate('validSession', false, false);
          break;

        case 'INDEX':
          if (isValidSession) {
            router.push('/analytics');
          } else {
            router.push('/auth/login');
          }
          break;
      }
    })();
  }, [isLogged]);

  return isLogged;
};

export default useRouteGuard;
