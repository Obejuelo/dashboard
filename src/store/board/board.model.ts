import { generateGUID } from 'utils/generate-GUID';

const cardModel = (card: any) => {
  return {
    id: card.id || generateGUID(),
    name: card.name || '',
    description: card.description || ''
  };
};

export { cardModel };
