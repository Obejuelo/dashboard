export enum boardAction {
  ADD_LIST = '[Board] Add List',
  GET_BOARD = '[Board] Get Board',
  ORDER_CARD = '[Board] Order Card',
  ADD_CARD = '[Board] Add New Card',
  UPDATE_CARD = '[Board] Update Card'
}

export type BoardReducer = {
  board: BoardReducerData;
};

export type BoardReducerData = {
  id: string;
  name: string;
  uri: string;
  lists: BoardListType[];
  cards: BoardListCardsType[];
};

export type BoardListType = {
  id: string;
  name: string;
  idCards: string[];
};

export type BoardListCardsType = {
  id: string;
  name: string;
  description: string;
};

export type AddNewList = {
  type: typeof boardAction.ADD_LIST;
  payload: {
    data: any;
  };
};

export type GetBoard = {
  type: typeof boardAction.GET_BOARD;
  payload: {
    data: BoardReducerData;
  };
};

export type OrderCard = {
  type: typeof boardAction.ORDER_CARD;
  payload: {
    data: any;
  };
};

export type AddCard = {
  type: typeof boardAction.ADD_CARD;
  payload: {
    data: BoardReducerData;
  };
};

export type UpdateCard = {
  type: typeof boardAction.UPDATE_CARD;
  payload: {
    data: any;
  };
};

export type BoardActionTypes =
  | AddNewList
  | GetBoard
  | OrderCard
  | AddCard
  | UpdateCard;
