import { boardAction, BoardActionTypes, BoardReducer } from './board.types';

const initialState: BoardReducer = {
  board: {
    id: '',
    name: '',
    uri: '',
    lists: [],
    cards: []
  }
};

export const boardReducer = (
  state = initialState,
  action: BoardActionTypes
): BoardReducer => {
  switch (action.type) {
    case boardAction.GET_BOARD:
      return {
        board: {
          ...state.board,
          ...action.payload.data
        }
      };
    case boardAction.ADD_LIST:
      return {
        board: {
          ...state.board,
          lists: action.payload.data
        }
      };
    case boardAction.ORDER_CARD:
      return {
        board: {
          ...state.board,
          lists: action.payload.data
        }
      };
    case boardAction.ADD_CARD:
      return {
        board: {
          ...state.board,
          ...action.payload.data
        }
      };
    default:
      return state;
  }
};
