import { Dispatch } from 'redux';
import axios from 'axios';

import { generateGUID } from 'utils/generate-GUID';
import { boardAction, BoardListType } from './board.types';
import { DropResult } from 'react-beautiful-dnd';
import { reorderQuoteMap } from './reorder';
import { cardModel } from './board.model';
import { showMessage } from 'store/configs/message/message.store.action';
import { closeDialog } from 'store/configs/dialog/dialog.store.actions';

export const getBoard = () => {
  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.get('/api/board/get-board');
      dispatch({ type: boardAction.GET_BOARD, payload: { data: resp.data } });
    } catch (e) {
      console.log(e);
    }
  };
};

export const addNewList = (boardId: string, listTitle: string) => {
  const data: BoardListType = {
    id: generateGUID(),
    name: listTitle,
    idCards: []
  };

  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.post('/api/board/list/new', {
        boardId,
        data
      });
      dispatch({ type: boardAction.ADD_LIST, payload: { data: resp.data } });
    } catch (e) {
      console.log(e);
    }
  };
};

export const addNewCard = (
  boardId: string,
  listId: string,
  cardTitle: string
) => {
  const data = cardModel({ name: cardTitle });

  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.post('/api/board/card/new', {
        boardId,
        listId,
        data
      });

      dispatch({
        type: boardAction.ADD_CARD,
        payload: { data: resp.data }
      });
    } catch (e) {
      console.log(e);
    }
  };
};

export const updateCard = (boardId: string, card: any) => {
  return async (dispatch: any) => {
    try {
      await axios.post('/api/board/card/update', { boardId, card });
      dispatch(getBoard());
      dispatch(
        showMessage({
          message: 'Card updated',
          variant: 'success',
          autoHideDuration: 2000,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right'
          }
        })
      );
      dispatch(closeDialog());
    } catch (e) {
      dispatch(
        showMessage({
          message: 'An error ocurred',
          variant: 'error'
        })
      );
    }
  };
};

export const reorderCard = (result: DropResult) => {
  return async (dispatch: Dispatch, getState: any) => {
    const { board } = getState().board;
    const { lists } = board;

    const ordered = reorderQuoteMap(lists, result.source, result.destination);

    try {
      await axios.post('/api/board/card/order', {
        boardId: board.id,
        lists: ordered
      });
      dispatch({ type: boardAction.ORDER_CARD, payload: { data: ordered } });
    } catch (e) {
      console.log(e);
    }
  };
};
