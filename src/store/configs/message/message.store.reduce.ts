import {
  MessageStateType,
  messageStateTypes,
  MessageTypeActions
} from './message.store.type';

const messageInitialState: MessageStateType = {
  state: false,
  options: {
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'center'
    },
    autoHideDuration: 6000,
    variant: 'success',
    message: ''
  }
};

export const messageReducer = (
  state = messageInitialState,
  action: MessageTypeActions
): MessageStateType => {
  switch (action.type) {
    case messageStateTypes.SHOW_MESSAGE:
      return {
        ...state,
        state: true,
        options: {
          ...messageInitialState.options,
          ...action.payload.options
        }
      };
    case messageStateTypes.HIDE_MESSAGE:
      return {
        ...state,
        state: false
      };
    default:
      return state;
  }
};
