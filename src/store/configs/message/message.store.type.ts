import { SnackbarProps } from '@mui/material';

export enum messageStateTypes {
  SHOW_MESSAGE = '[Message] Open alert message',
  HIDE_MESSAGE = '[Message] Close alert message'
}

export type MessageStateType = {
  state?: boolean;
  options: MessageOptionsType;
};

export type MessageOptionsType = SnackbarProps & {
  variant: 'success' | 'warning' | 'info' | 'error';
};

export type showMessage = {
  type: typeof messageStateTypes.SHOW_MESSAGE;
  payload: {
    options: SnackbarProps;
  };
};

export type hideMessage = {
  type: typeof messageStateTypes.HIDE_MESSAGE;
};

export type MessageTypeActions = showMessage | hideMessage;
