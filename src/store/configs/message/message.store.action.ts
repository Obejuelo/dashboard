import { MessageOptionsType, messageStateTypes } from './message.store.type';

export const showMessage = (options: MessageOptionsType) => {
  return {
    type: messageStateTypes.SHOW_MESSAGE,
    payload: { options }
  };
};

export const hideMessage = () => {
  return {
    type: messageStateTypes.HIDE_MESSAGE
  };
};
