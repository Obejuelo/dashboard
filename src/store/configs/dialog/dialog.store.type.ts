import { DialogProps } from '@mui/material';

export enum dialogActionTypes {
  OPEN_DIALOG = '[Dialog] Open dialog',
  CLOSE_DIALOG = '[Dialog] Close dialog',
  OPEN_DRAWER = '[Drawer] Open drawer'
}

export type DialogState = {
  state: boolean;
  options: DialogOptionsType;
  drawerSat: boolean;
};

export type DialogOptionsType = Partial<Omit<DialogProps, 'open'>>;

export type openDialog = {
  type: typeof dialogActionTypes.OPEN_DIALOG;
  options: DialogOptionsType;
};

export type closeDialog = {
  type: typeof dialogActionTypes.CLOSE_DIALOG;
};

export type openDrawer = {
  type: typeof dialogActionTypes.OPEN_DRAWER;
  payload: {
    data: boolean;
  };
};

export type DialogActions = openDialog | closeDialog | openDrawer;
