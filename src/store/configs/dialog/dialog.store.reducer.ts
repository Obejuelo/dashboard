import {
  DialogActions,
  dialogActionTypes,
  DialogState
} from './dialog.store.type';

const dialogInitialState: DialogState = {
  state: false,
  drawerSat: false,
  options: {
    children: ''
  }
};

export const dialogReducer = (
  state = dialogInitialState,
  action: DialogActions
): DialogState => {
  switch (action.type) {
    case dialogActionTypes.OPEN_DIALOG:
      return {
        ...state,
        state: true,
        options: {
          ...state.options,
          ...action.options
        }
      };
    case dialogActionTypes.CLOSE_DIALOG:
      return {
        ...state,
        state: false,
        options: {
          fullScreen: false
        }
      };
    case dialogActionTypes.OPEN_DRAWER:
      return {
        ...state,
        drawerSat: action.payload.data
      };
    default:
      return state;
  }
};
