import { dialogActionTypes, DialogOptionsType } from './dialog.store.type';

export const openDialog = (options: DialogOptionsType) => {
  return {
    type: dialogActionTypes.OPEN_DIALOG,
    options
  };
};

export const closeDialog = () => {
  return {
    type: dialogActionTypes.CLOSE_DIALOG
  };
};

export const openDrawer = (option: boolean) => {
  return {
    type: dialogActionTypes.OPEN_DRAWER,
    payload: {
      data: option
    }
  };
};
