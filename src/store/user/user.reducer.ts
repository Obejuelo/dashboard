import {
  userActionTypes,
  UserActionTypes,
  UserReducerState
} from './user.type';

const initialState: UserReducerState = {
  isLogged: false,
  data: {
    displayName: '',
    email: '',
    theme: ''
  }
};

export const userReducer = (
  state = initialState,
  action: UserActionTypes
): UserReducerState => {
  switch (action.type) {
    case userActionTypes.SET_AUTHENTICATION:
      return {
        ...state,
        isLogged: action.payload.data
      };
    case userActionTypes.SET_USER_DATA:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.payload.data
        }
      };
    default:
      return state;
  }
};
