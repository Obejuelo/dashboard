export enum userActionTypes {
  SET_AUTHENTICATION = '[User] Set Authentication',
  SET_USER_DATA = '[User] Set User Info',
  USER_LOGOUT = '[User] User Logout'
}

export type UserReducerState = {
  isLogged?: boolean;
  data: UserDataType;
};

export type UserDataType = {
  displayName?: string;
  email?: string;
  theme?: string;
};

export type SetUserData = {
  type: typeof userActionTypes.SET_USER_DATA;
  payload: {
    data: UserDataType;
  };
};

export type SetUserAuthentication = {
  type: typeof userActionTypes.SET_AUTHENTICATION;
  payload: {
    data: boolean;
  };
};

export type UserLogout = {
  type: typeof userActionTypes.USER_LOGOUT;
};

export type UserActionTypes = SetUserAuthentication | SetUserData | UserLogout;
