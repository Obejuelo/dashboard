import { Dispatch } from 'redux';
import { mutate } from 'swr';
import Router from 'next/router';

import { AuthService } from 'services/auth/auth.service';
import { userActionTypes, UserDataType } from './user.type';

export const setUserAuthentication = (data: boolean) => ({
  type: userActionTypes.SET_AUTHENTICATION,
  payload: { data }
});

export const setUserData = (data: UserDataType) => ({
  type: userActionTypes.SET_USER_DATA,
  payload: { data }
});

export const userLogout = () => {
  const authService = new AuthService();

  return async (dispatch: Dispatch) => {
    authService.setSession(null);
    dispatch(setUserAuthentication(false));
    mutate('validSession', false, false);
    Router.push('/auth/login');
  };
};
