import {
  analyticsActions,
  AnalyticsActionTypes,
  AnalyticsReducer
} from './analytics.type';

const initialState: AnalyticsReducer = {
  data: null
};

export const analyticsReducer = (
  state = initialState,
  action: AnalyticsActionTypes
): AnalyticsReducer => {
  switch (action.type) {
    case analyticsActions.GET_WIDGETS:
      return {
        ...state,
        data: action.payload.data
      };
    default:
      return state;
  }
};
