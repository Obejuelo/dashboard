export enum analyticsActions {
  GET_WIDGETS = '[Analytics] Get Widgets'
}

export type AnalyticsReducer = {
  data: any;
};

export type GetAnalyticWidgets = {
  type: typeof analyticsActions.GET_WIDGETS;
  payload: {
    data: any;
  };
};

export type AnalyticsActionTypes = GetAnalyticWidgets;
