import axios from 'axios';
import { Dispatch } from 'redux';

import { analyticsActions } from './analytics.type';

export const getAnalyticWidgets = () => {
  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.get('/api/analytics/widgets');
      dispatch({
        type: analyticsActions.GET_WIDGETS,
        payload: { data: resp.data }
      });
    } catch (e) {
      console.log(e);
    }
  };
};
