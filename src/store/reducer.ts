import { AnyAction, combineReducers, Reducer } from 'redux';
import { HYDRATE } from 'next-redux-wrapper';

import { authReducer } from './auth/auth.reducer';
import { userReducer } from './user/user.reducer';
import { messageReducer } from './configs/message/message.store.reduce';
import { dialogReducer } from './configs/dialog/dialog.store.reducer';
import { analyticsReducer } from './analytics/analytics.reducer';
import { boardReducer } from './board/board.reducer';
import { chatAppReducer } from './chat/chat.reducer';

export const combinedReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  dialog: dialogReducer,
  message: messageReducer,
  analytics: analyticsReducer,
  board: boardReducer,
  chat: chatAppReducer
});

export const rootReducer: Reducer = (
  state: AppState,
  action: AnyAction
): AppState => {
  if (action.type === HYDRATE) {
    return {
      ...state,
      ...action.payload
    };
  }

  return combinedReducer(state, action);
};

export type AppState = ReturnType<typeof combinedReducer>;
