import axios from 'axios';
import { Dispatch } from 'redux';
import { chatAppActions } from './chat.type';

// CONTACTS
export const getContacts = () => {
  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.get('/api/chat/contacts');
      dispatch({
        type: chatAppActions.GET_CONTACTS,
        payload: {
          data: resp.data
        }
      });
    } catch (e) {
      console.log(e);
    }
  };
};

export const setSelectedContactId = (contactId: string) => ({
  type: chatAppActions.SET_SELECTED_CONTACT_ID,
  payload: {
    data: contactId
  }
});

export const removeSelectedContactId = () => ({
  type: chatAppActions.REMOVE_SELECTED_CONTACT_ID
});

// USER
export const getChatUserData = () => {
  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.get('/api/chat/user');
      dispatch({
        type: chatAppActions.GET_USER_DATA,
        payload: {
          data: resp.data
        }
      });
    } catch (e) {
      console.log(e);
    }
  };
};

export const updateChatUserData = () => {
  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.post('/api/chat/user-data');
      dispatch({
        type: chatAppActions.UPDATE_USER_DATA,
        payload: {
          data: resp.data
        }
      });
    } catch (e) {
      console.log(e);
    }
  };
};

// CHAT
export const getChat = (contactId: string) => {
  return async (dispatch: Dispatch, getState: any) => {
    const { id: userId } = getState().chat.user;

    try {
      const resp = await axios.get('/api/chat/get-chat', {
        params: {
          contactId,
          userId
        }
      });

      dispatch(setSelectedContactId(contactId));
      dispatch({
        type: chatAppActions.GET_CHAT,
        payload: {
          data: resp.data.chat
        }
      });
    } catch (e) {
      console.log(e);
    }
  };
};

export const removeChat = () => ({
  type: chatAppActions.REMOVE_CHAT
});

export const sendMessage = (
  messageText: string,
  chatId: string,
  userId: string
) => {
  const message = {
    who: userId,
    message: messageText,
    time: new Date()
  };

  return async (dispatch: Dispatch) => {
    try {
      const resp = await axios.post('/api/chat/send-message', {
        chatId,
        message
      });

      dispatch({
        type: chatAppActions.SEND_MESSAGE,
        payload: {
          data: resp.data.message
        }
      });
    } catch (e) {
      console.log(e);
    }
  };
};
