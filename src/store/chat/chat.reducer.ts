import _ from 'lodash';

import { ChatActionTypes, chatAppActions, ChatAppReducer } from './chat.type';

const initialState: ChatAppReducer = {
  user: {
    id: '',
    name: '',
    status: '',
    mood: '',
    chatList: []
  },
  contacts: {
    entities: [],
    selectedContactId: ''
  },
  chat: {
    id: '',
    dialog: []
  }
};

export const chatAppReducer = (
  state = initialState,
  action: ChatActionTypes
): ChatAppReducer => {
  switch (action.type) {
    case chatAppActions.GET_CONTACTS:
      return {
        ...state,
        contacts: {
          ...state.contacts,
          entities: action.payload.data
        }
      };
    case chatAppActions.SET_SELECTED_CONTACT_ID:
      return {
        ...state,
        contacts: {
          ...state.contacts,
          selectedContactId: action.payload.data
        }
      };
    case chatAppActions.REMOVE_SELECTED_CONTACT_ID:
      return {
        ...state,
        contacts: {
          ...state.contacts,
          selectedContactId: ''
        }
      };
    case chatAppActions.GET_CHAT:
      return {
        ...state,
        chat: {
          ...action.payload.data
        }
      };
    case chatAppActions.REMOVE_CHAT:
      return {
        ...state,
        chat: initialState.chat
      };
    case chatAppActions.SEND_MESSAGE:
      return {
        ...state,
        chat: {
          ...state.chat,
          dialog: [...state.chat.dialog, action.payload.data]
        }
      };
    case chatAppActions.GET_USER_DATA:
      return {
        ...state,
        user: action.payload.data
      };
    case chatAppActions.UPDATE_USER_DATA:
      return {
        ...state,
        user: action.payload.data
      };
    default:
      return state;
  }
};

export const getUpdatedUser = (state: ChatAppReducer, action: any) => {
  const newUserData = _.merge({}, state);
  const userChatData = newUserData.user.chatList.find(
    _chat => _chat.contactId === action.userChatData.contactId
  );

  if (userChatData) {
    newUserData.user.chatList = newUserData.user.chatList.map(_chat =>
      _chat.contactId === action.userChatData.contactId
        ? action.userChatData
        : _chat
    );
  } else {
    newUserData.user.chatList = [
      action.userChatData,
      ...newUserData.user.chatList
    ];
  }

  return newUserData;
};
