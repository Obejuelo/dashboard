export enum chatAppActions {
  GET_CONTACTS = '[Chat App] Get Contacts',
  SET_SELECTED_CONTACT_ID = '[Chat App] Set Selected Contact Id',
  REMOVE_SELECTED_CONTACT_ID = '[Chat App] Remove Selected Contact Id',
  GET_USER_DATA = '[Chat App] Get User Data',
  UPDATE_USER_DATA = '[Chat App] Update User Data',
  GET_CHAT = '[Chat App] Get Chat',
  REMOVE_CHAT = '[Chat App] Remove Chat',
  SEND_MESSAGE = '[Chat App] Send Message'
}

export type ChatAppReducer = {
  user: ChatAppUserType;
  chat: {
    id: string;
    dialog: ChatAppChatDialogType[];
  };
  contacts: {
    entities: ChatAppContactsType[];
    selectedContactId: string;
  };
};

export type ChatAppType = {
  contacts: ChatAppContactsType[];
  chats: ChatAppChatType[];
  user: ChatAppUserType[];
};

export type ChatAppContactsType = {
  id: string;
  name: string;
  avatar?: string;
  status: string;
  mood: string;
  unread?: string;
};

export type ChatAppChatType = {
  id: string;
  dialog: ChatAppChatDialogType[];
};

export type ChatAppChatDialogType = {
  who: string;
  message: string;
  time: string;
};

export type ChatAppUserType = {
  id: string;
  name: string;
  status: string;
  mood: string;
  chatList: ChatAppUserChatListType[];
};

export type ChatAppUserChatListType = {
  chatId: string;
  contactId: string;
  lastMessageTime: string;
};

// ACTIONS
export type GetContacts = {
  type: typeof chatAppActions.GET_CONTACTS;
  payload: {
    data: ChatAppContactsType[];
  };
};

export type SetSelectedContactId = {
  type: typeof chatAppActions.SET_SELECTED_CONTACT_ID;
  payload: {
    data: string;
  };
};

export type RemoveSelectedContactId = {
  type: typeof chatAppActions.REMOVE_SELECTED_CONTACT_ID;
};

export type GetUserData = {
  type: typeof chatAppActions.GET_USER_DATA;
  payload: {
    data: ChatAppUserType;
  };
};

export type UpdateUserData = {
  type: typeof chatAppActions.UPDATE_USER_DATA;
  payload: {
    data: any;
  };
};

export type GetChat = {
  type: typeof chatAppActions.GET_CHAT;
  payload: {
    data: any;
  };
};

export type RemoveChat = {
  type: typeof chatAppActions.REMOVE_CHAT;
};

export type SendMessage = {
  type: typeof chatAppActions.SEND_MESSAGE;
  payload: {
    data: any;
  };
};

export type ChatActionTypes =
  | GetContacts
  | SetSelectedContactId
  | RemoveSelectedContactId
  | GetUserData
  | UpdateUserData
  | GetChat
  | RemoveChat
  | SendMessage;
