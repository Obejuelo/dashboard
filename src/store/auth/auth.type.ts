export enum AuthActionTypes {
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_ERROR = '[Auth] Login Error',
  REGISTER_SUCCESS = '[Auth] Register Success'
}

export type AuthReducer = {
  success: boolean;
  error: AuthReducerErrorType;
};

export type AuthReducerErrorType = {
  username: string | null;
  password: string | null;
};

export type LoginSuccess = {
  type: typeof AuthActionTypes.LOGIN_SUCCESS;
  payload: {
    data: boolean;
  };
};

export type LoginError = {
  type: typeof AuthActionTypes.LOGIN_ERROR;
  payload: {
    data: AuthReducerErrorType;
  };
};

export type RegisterSuccess = {
  type: typeof AuthActionTypes.REGISTER_SUCCESS;
  payload: {
    data: any;
  };
};

export type AuthActions = LoginSuccess | LoginError | RegisterSuccess;
