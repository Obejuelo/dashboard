import { Dispatch } from 'redux';
import { mutate } from 'swr';
import Router from 'next/router';

import {
  LoginRequestData,
  RegisterRequestData
} from 'services/auth/auth.service.types';
import { AuthService } from '../../services/auth/auth.service';
import { setUserAuthentication, setUserData } from 'store/user/user.action';
import { AuthActionTypes } from './auth.type';
import { showMessage } from 'store/configs/message/message.store.action';

export const submitLogin = (request: LoginRequestData) => {
  const authService = new AuthService();

  return async (dispatch: Dispatch) => {
    try {
      const resp = await authService.loginWithEmail(request);
      if (resp.access_token) {
        dispatch({
          type: AuthActionTypes.LOGIN_SUCCESS,
          payload: { data: true }
        });

        dispatch(setUserAuthentication(true));
        dispatch(setUserData(resp.user.data));
        mutate('validSession', true, false);
        Router.push('/analytics');
      }
    } catch (e: any) {
      dispatch(
        showMessage({ message: 'Invalid email or password', variant: 'error' })
      );
      dispatch({
        type: AuthActionTypes.LOGIN_ERROR,
        payload: { data: request }
      });
    }
  };
};

export const registerWithEmail = (request: RegisterRequestData) => {
  const authService = new AuthService();

  return async (dispatch: Dispatch) => {
    try {
      const resp = await authService.register(request);
      if (resp.access_token) {
        dispatch(setUserAuthentication(true));
        dispatch(setUserData(resp.user.data));
        mutate('validSession', true, false);
        Router.push('/analytics');
      }
    } catch (e) {
      dispatch(showMessage({ message: 'An error ocurred', variant: 'error' }));
    }
  };
};
