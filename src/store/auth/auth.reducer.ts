import { AuthActions, AuthActionTypes, AuthReducer } from './auth.type';

const initialState: AuthReducer = {
  success: true,
  error: {
    username: null,
    password: null
  }
};

export const authReducer = (
  state = initialState,
  action: AuthActions
): AuthReducer => {
  switch (action.type) {
    case AuthActionTypes.LOGIN_SUCCESS:
      return {
        ...initialState,
        success: true
      };
    case AuthActionTypes.LOGIN_ERROR:
      return {
        success: false,
        error: {
          ...action.payload.data
        }
      };
    default:
      return state;
  }
};
