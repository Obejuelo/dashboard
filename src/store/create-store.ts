import { applyMiddleware, createStore, Store } from 'redux';
import { MakeStore, createWrapper } from 'next-redux-wrapper';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunkMiddleware from 'redux-thunk';

import { rootReducer, AppState } from './reducer';

const middleware = [thunkMiddleware];

export const makeStore: MakeStore<AppState> = (): Store => {
  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middleware))
  );

  return store;
};

export const wrapper = createWrapper(makeStore, { debug: false });
