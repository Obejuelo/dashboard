import { sidebarMenu } from 'constants/sidebar-menu';

export const appConfig = {
  api: {
    url: process.env.NEXT_PUBLIC_API_URL
  },
  sidebarMenu
};
