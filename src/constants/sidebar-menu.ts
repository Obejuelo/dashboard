export const sidebarMenu = [
  {
    path: '/analytics',
    id: 'analytics',
    name: 'Analytics',
    icon: 'analytics'
  },
  {
    path: '/board',
    id: 'board',
    name: 'Board',
    icon: 'table_chart'
  },
  {
    path: '/chat',
    id: 'chat',
    name: 'Chat app',
    icon: 'chat'
  }
  // {
  //   path: '/notes',
  //   id: 'notes',
  //   name: 'Notes',
  //   icon: 'notes'
  // }
];
