export const filterArrayByString = (mainArr: any[], searchText: string) => {
  if (searchText === '') {
    return mainArr;
  }

  searchText = searchText.toLowerCase();

  return mainArr.filter(itemObj => {
    return searchInObj(itemObj, searchText);
  });
};

export const searchInObj = (itemObj: any, searchText: string) => {
  for (const prop in itemObj) {
    // eslint-disable-next-line no-prototype-builtins
    if (!itemObj.hasOwnProperty(prop)) {
      continue;
    }

    const value = itemObj[prop];

    if (typeof value === 'string') {
      if (searchInString(value, searchText)) {
        return true;
      }
    } else if (Array.isArray(value)) {
      if (searchInArray(value, searchText)) {
        return true;
      }
    }

    if (typeof value === 'object') {
      if (searchInObj(value, searchText)) {
        return true;
      }
    }
  }
};

export const searchInArray = (arr: any[], searchText: string) => {
  // eslint-disable-next-line no-unused-vars
  for (const value of arr) {
    if (typeof value === 'string') {
      if (searchInString(value, searchText)) {
        return true;
      }
    }

    if (typeof value === 'object') {
      if (searchInObj(value, searchText)) {
        return true;
      }
    }
  }
};

export const searchInString = (value: any, searchText: string) => {
  return value.toLowerCase().includes(searchText);
};
