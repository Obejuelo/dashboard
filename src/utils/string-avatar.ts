export const stringAvatar = (name: string): string => {
  const nameTmp = name.split(' ');
  return nameTmp.length === 1
    ? nameTmp[0][0]
    : `${nameTmp[0][0]}${nameTmp[1][0]}`;
};
