export class EventEmitter {
  private events: { [key: string]: any } = {};

  constructor() {
    this.events = {};
  }

  private getEventListByName(eventName: string) {
    if (typeof this.events[eventName] === 'undefined') {
      this.events[eventName] = new Set();
    }
    return this.events[eventName];
  }

  public on(eventName: string, fn: () => void) {
    this.getEventListByName(eventName).add(fn);
  }

  public once(eventName: string, fn: () => void) {
    const onceFn = (...args: []) => {
      this.removeListener(eventName, onceFn);
      fn.apply(self, args);
    };
    this.on(eventName, onceFn);
  }

  public emit(eventName: string, args?: any) {
    this.getEventListByName(eventName).forEach((fn: () => void) => {
      fn.apply(this, args as []);
    });
  }

  public removeListener(eventName: string, fn: () => void) {
    this.getEventListByName(eventName).delete(fn);
  }
}
