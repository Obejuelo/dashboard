export type Response<T> = {
  statusCode: number;
  status?: number;
  data: T;
};
