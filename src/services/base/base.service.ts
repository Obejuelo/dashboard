import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

import { appConfig } from '../../core/config';
import { AuthService } from '../auth/auth.service';

export class BaseService {
  private http: AxiosInstance;

  constructor(endpoint: string) {
    const headers: any = {
      'Content-Type': 'application/json'
    };

    const token = AuthService.getToken();
    if (token) {
      headers['Authorization'] = `Bearer ${token}`;
    }

    const axiosDefaultConfig: AxiosRequestConfig = {
      baseURL: `${appConfig.api.url}${endpoint}`,
      responseType: 'json',
      headers
    };

    const customAxios = axios.create(axiosDefaultConfig);
    this.http = customAxios;
  }

  public async get<R>(url: string, config?: AxiosRequestConfig): Promise<R> {
    const response = await this.http.get(url, config);
    return response.data;
  }

  public async post<R>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig
  ): Promise<R> {
    const response = await this.http.post(url, data, config);
    return response.data;
  }

  public async put<R>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig
  ): Promise<R> {
    const response = await this.http.put(url, data, config);
    return response.data;
  }

  public async delete<R>(url: string, config?: AxiosRequestConfig): Promise<R> {
    const response = await this.http.delete(url, config);
    return response.data;
  }
}
