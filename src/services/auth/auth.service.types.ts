export type LoginRequestData = {
  email: string;
  password: string;
};

export type LoginWithEmailResponse = {
  access_token: string;
  user: {
    uuid: string;
    from: string;
    role: string;
    data: {
      displayName: string;
      theme: string;
      email: string;
    };
  };
};

export type RegisterRequestData = {
  email: string;
  name: string;
  password: string;
};
