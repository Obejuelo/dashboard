import axios from 'axios';
import jwt from 'jsonwebtoken';

import { BaseService } from '../base/base.service';
import {
  LoginRequestData,
  LoginWithEmailResponse,
  RegisterRequestData
} from './auth.service.types';

export class AuthService {
  private http: BaseService;
  private endpoint = '/api/auth'; //@TODO change this url for your API

  constructor() {
    this.http = new BaseService(this.endpoint);
  }

  public setSession(token: string | null): void {
    if (token) {
      localStorage.setItem('access_token', token);
    } else {
      localStorage.removeItem('access_token');
    }
  }

  public static getToken(): string {
    const accessToken = localStorage.getItem('access_token');
    return accessToken as string;
  }

  public async loginWithEmail(
    data: LoginRequestData
  ): Promise<LoginWithEmailResponse> {
    const resp = await axios.post<LoginWithEmailResponse>(
      '/api/auth/login',
      data
    );

    this.setSession(resp.data.access_token);
    return resp.data;
  }

  public async register(
    data: RegisterRequestData
  ): Promise<LoginWithEmailResponse | any> {
    try {
      const response = await axios.post<LoginWithEmailResponse>(
        '/api/auth/register',
        data
      );
      this.setSession(response.data.access_token);
      return response.data;
    } catch (e) {
      return e;
    }
  }

  public signInWithToken = (): boolean => {
    const token = AuthService.getToken();
    const expired = this.isAuthTokenValid(token);

    if (!expired) {
      this.setSession(null);
      return false;
    }
    return true;
  };

  public isAuthTokenValid = (token: string): boolean => {
    if (!token) return false;

    const decoded = jwt.decode(token);
    const expired = decoded as any;
    const currentTime = Date.now() / 1000;

    if (expired.exp < currentTime) {
      console.warn('access token expired');
      return false;
    } else {
      return true;
    }
  };

  public getUserData = async () => {
    const token = AuthService.getToken();
    const data: any = jwt.decode(token);

    try {
      const resp = await axios.post('/api/auth/user', { uuid: data.id });
      return resp;
    } catch (e) {
      return e;
    }
  };
}
