import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  card: {
    border: `solid 1px ${theme.palette.divider}`,
    padding: '1rem'
  },
  marginBottom: {
    marginBottom: '1rem',
    [theme.breakpoints.up('lg')]: {
      marginBottom: 0
    }
  }
}));
