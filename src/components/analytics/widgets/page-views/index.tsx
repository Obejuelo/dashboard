import React, { useState } from 'react';
import { Box, Button, Card, Typography, useTheme } from '@mui/material';
import { Line } from 'react-chartjs-2';
import clsx from 'clsx';
import _ from 'lodash';

import { useStyles } from '../style';

interface AnalyticPageViewsProps {
  data: any;
}

const AnalyticPageViews: React.FC<AnalyticPageViewsProps> = props => {
  const classes = useStyles();
  const theme = useTheme();
  const [dataset, setDataset] = useState('today');
  const data = _.merge({}, props.data);

  return (
    <Card elevation={0} className={clsx(classes.card, classes.marginBottom)}>
      <Box display="flex" justifyContent="space-between" mb={2}>
        <Typography variant="h4" color="textSecondary">
          Page Views
        </Typography>
        <Box display="flex" alignItems="center">
          {Object.keys(data.datasets).map(key => (
            <Button
              key={key}
              className="py-8 px-12"
              size="small"
              onClick={() => setDataset(key)}
              disabled={key === dataset}
            >
              {key}
            </Button>
          ))}
        </Box>
      </Box>
      <Box height="25.438rem">
        <Line
          data={{
            labels: data.labels,
            datasets: data.datasets[dataset].map((obj: any, index: number) => {
              const palette =
                theme.palette[index === 0 ? 'primary' : 'secondary'];
              return {
                ...obj,
                borderColor: palette.main,
                backgroundColor: palette.main,
                pointBackgroundColor: palette.dark,
                pointHoverBackgroundColor: palette.main,
                pointBorderColor: palette.light,
                pointHoverBorderColor: palette.light
              };
            })
          }}
          options={data.options}
        />
      </Box>
    </Card>
  );
};

export default AnalyticPageViews;
