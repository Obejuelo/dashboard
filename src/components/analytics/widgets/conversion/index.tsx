import React from 'react';
import { Box, Card, Typography, useTheme, Icon } from '@mui/material';
import { Bar } from 'react-chartjs-2';
import clsx from 'clsx';

import { useStyles } from '../style';

interface AnalyticConversionProps {
  data: any;
}

const AnalyticConversion: React.FC<AnalyticConversionProps> = ({ data }) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Card elevation={0} className={clsx(classes.card, classes.marginBottom)}>
      <Typography variant="h4" color="textSecondary">
        Conversion
      </Typography>
      <Box mt={2} display="flex">
        <Typography variant="h3">{data.conversion.value}</Typography>
        <Box display="flex">
          <Box display="flex" alignItems="center">
            {data.conversion.ofTarget > 0 && (
              <Icon style={{ color: 'green' }}>trending_up</Icon>
            )}
            {data.conversion.ofTarget < 0 && (
              <Icon style={{ color: 'red' }}>trending_down</Icon>
            )}
            <Typography>{data.conversion.ofTarget}%</Typography>
          </Box>
        </Box>
      </Box>
      <Box height="12rem">
        <Bar
          data={{
            labels: data.labels,
            datasets: data.datasets.map((obj: any) => ({
              ...obj,
              borderColor: theme.palette.warning.main,
              backgroundColor: theme.palette.warning.main
            }))
          }}
          options={data.options}
        />
      </Box>
    </Card>
  );
};

export default AnalyticConversion;
