import React from 'react';
import { Card, Typography, useTheme, Icon } from '@mui/material';
import { Bar } from 'react-chartjs-2';

import { useStyles } from '../style';
import { Box } from '@mui/system';

interface AnalyticVisitorsProps {
  data: any;
}

const AnalyticVisitors: React.FC<AnalyticVisitorsProps> = ({ data }) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Card elevation={0} className={classes.card}>
      <Typography variant="h4" color="textSecondary">
        Visits
      </Typography>
      <Box mt={2} display="flex">
        <Typography variant="h3">{data.visits.value}</Typography>
        <Box display="flex">
          <Box display="flex" alignItems="center">
            {data.visits.ofTarget > 0 && (
              <Icon style={{ color: 'green' }}>trending_up</Icon>
            )}
            {data.visits.ofTarget < 0 && (
              <Icon style={{ color: 'red' }}>trending_down</Icon>
            )}
            <Typography>{data.visits.ofTarget}%</Typography>
          </Box>
        </Box>
      </Box>
      <Box height="12rem">
        <Bar
          data={{
            labels: data.labels,
            datasets: data.datasets.map((obj: any) => ({
              ...obj,
              borderColor: theme.palette.info.main,
              backgroundColor: theme.palette.info.main
            }))
          }}
          options={data.options}
        />
      </Box>
    </Card>
  );
};

export default AnalyticVisitors;
