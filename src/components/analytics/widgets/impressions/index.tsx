import React from 'react';
import { Card, Typography, useTheme, Icon } from '@mui/material';
import { Line } from 'react-chartjs-2';

import { useStyles } from '../style';
import { Box } from '@mui/system';

interface AnalyticImpressionsProps {
  data: any;
}

const AnalyticImpressions: React.FC<AnalyticImpressionsProps> = ({ data }) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Card elevation={0} className={classes.card}>
      <Typography variant="h4" color="textSecondary">
        Impressions
      </Typography>
      <Box mt={2} display="flex">
        <Typography variant="h3">{data.impressions.value}</Typography>
        <Box display="flex">
          <Box display="flex" alignItems="center">
            {data.impressions.ofTarget > 0 && (
              <Icon style={{ color: 'green' }}>trending_up</Icon>
            )}
            {data.impressions.ofTarget < 0 && (
              <Icon style={{ color: 'red' }}>trending_down</Icon>
            )}
            <Typography>{data.impressions.ofTarget}%</Typography>
          </Box>
        </Box>
      </Box>
      <Box height="12rem">
        <Line
          data={{
            labels: data.labels,
            datasets: data.datasets.map((obj: any) => ({
              ...obj,
              borderColor: theme.palette.secondary.main
            }))
          }}
          options={data.options}
        />
      </Box>
    </Card>
  );
};

export default AnalyticImpressions;
