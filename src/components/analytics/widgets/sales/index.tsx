import React, { useState } from 'react';
import {
  Box,
  Button,
  Card,
  Tab,
  Tabs,
  Typography,
  useTheme
} from '@mui/material';
import { Line } from 'react-chartjs-2';

import { useStyles } from '../style';

interface AnalyticSalesProps {
  data: any;
}

const AnalyticSales: React.FC<AnalyticSalesProps> = ({ data }) => {
  const theme = useTheme();
  const classes = useStyles();
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <Card elevation={0} className={classes.card}>
      <Typography variant="h4" color="textSecondary">
        Sales info
      </Typography>
      <Typography>Sales by category</Typography>
      <Box my={3}>
        <Typography variant="h1">{data.today}</Typography>
      </Box>
      <Box width={1} display="flex" justifyContent="center">
        <Tabs value={tabIndex} onChange={(e, idx) => setTabIndex(idx)}>
          <Tab label="1 day" />
          <Tab label="1 week" />
          <Tab label="1 month" />
        </Tabs>
      </Box>
      <Line
        data={{
          labels: data.labels,
          datasets: data.datasets[tabIndex].map((obj: any) => ({
            ...obj,
            borderColor: theme.palette.secondary.main
          }))
        }}
        options={data.options}
      />
      <Box display="flex" justifyContent="flex-end">
        <Button>See more</Button>
      </Box>
    </Card>
  );
};

export default AnalyticSales;
