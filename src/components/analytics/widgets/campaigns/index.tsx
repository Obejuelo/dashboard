import React from 'react';
import {
  Box,
  Button,
  Card,
  Icon,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography
} from '@mui/material';

import { useStyles } from '../style';

interface AnalyticCampaignsProps {
  data: any;
}

const AnalyticCampaigns: React.FC<AnalyticCampaignsProps> = ({ data }) => {
  const classes = useStyles();

  return (
    <Card elevation={0} className={classes.card}>
      <Box display="flex" justifyContent="space-between">
        <Typography variant="h4" color="textSecondary">
          Campaigns
        </Typography>
        <IconButton size="small">
          <Icon>more_vert</Icon>
        </IconButton>
      </Box>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <TableCell align="right">Clicks</TableCell>
            <TableCell align="right">Conv</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.rows.map((row: any, idx: number) => (
            <TableRow key={idx} hover>
              <TableCell>{row.title}</TableCell>
              <TableCell align="right">{row.clicks}</TableCell>
              <TableCell align="right">{row.conversion}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Box display="flex" justifyContent="flex-end" mt={1}>
        <Button>See more</Button>
      </Box>
    </Card>
  );
};

export default AnalyticCampaigns;
