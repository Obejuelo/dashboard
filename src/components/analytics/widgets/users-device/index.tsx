import React, { useState } from 'react';
import {
  Box,
  Button,
  Card,
  Divider,
  FormControl,
  Grid,
  Icon,
  MenuItem,
  Select,
  Typography,
  useTheme
} from '@mui/material';
import { Doughnut } from 'react-chartjs-2';

import { useStyles } from '../style';

interface UsersDeviceProps {
  data: any;
}

const UsersDevice: React.FC<UsersDeviceProps> = ({ data }) => {
  const theme = useTheme();
  const classes = useStyles();
  const [dataset, setDataset] = useState('Today');

  return (
    <Card elevation={0} className={classes.card}>
      <Box mb={1}>
        <Typography variant="h4" color="textSecondary">
          Users by device
        </Typography>
      </Box>
      <Box height="14rem">
        <Doughnut
          data={{
            labels: data.labels,
            datasets: data.datasets[dataset].map((obj: any) => ({
              ...obj,
              borderColor: theme.palette.divider,
              backgroundColor: [
                theme.palette.primary.dark,
                theme.palette.primary.main,
                theme.palette.primary.light
              ],
              hoverBackgroundColor: [
                theme.palette.secondary.dark,
                theme.palette.secondary.main,
                theme.palette.secondary.light
              ]
            }))
          }}
          options={data.options}
        />
      </Box>
      <Box my={1}>
        <Grid container spacing={1}>
          {data.labels.map((label: any, idx: number) => (
            <Grid
              key={label}
              item
              xs={4}
              container
              alignItems="center"
              justifyContent="center"
            >
              <Box>
                <Typography color="textSecondary">{label}</Typography>
                <Typography variant="h5">
                  {data.datasets[dataset][0].data[idx]}%
                </Typography>
              </Box>
              <Box
                width={1}
                display="flex"
                alignItems="center"
                justifyContent="center"
              >
                {data.datasets[dataset][0].change[idx] < 0 && (
                  <Icon style={{ color: 'red', fontSize: 18 }}>
                    arrow_downward
                  </Icon>
                )}
                {data.datasets[dataset][0].change[idx] > 0 && (
                  <Icon style={{ color: 'green', fontSize: 18 }}>
                    arrow_upward
                  </Icon>
                )}
                <Typography>
                  {data.datasets[dataset][0].change[idx]}%
                </Typography>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Divider />
      <Box
        mt={1.5}
        display="flex"
        alignItems="center"
        justifyContent="space-between"
      >
        <FormControl>
          <Select
            size="small"
            value={dataset}
            onChange={e => setDataset(e.target.value)}
          >
            {Object.keys(data.datasets).map(key => (
              <MenuItem key={key} value={key}>
                {key}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button size="small">See more</Button>
      </Box>
    </Card>
  );
};

export default UsersDevice;
