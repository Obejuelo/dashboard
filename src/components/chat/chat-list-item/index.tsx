import React from 'react';
import {
  Box,
  Icon,
  ListItemButton,
  ListItemText,
  Typography
} from '@mui/material';
import clsx from 'clsx';

import { useStyles } from './style';
import { AvatarName } from 'components/common/avatar/avatar-name';
import { ChatAppContactsType } from 'core/fake-db/db/types/chat.type';

interface ChatListItemProps {
  item?: ChatAppContactsType;
  onContactClick: () => void;
}

const ChatListItem: React.FC<ChatListItemProps> = props => {
  const { item, onContactClick } = props;
  const classes = useStyles();

  return (
    <ListItemButton className={clsx(classes.listItem)} onClick={onContactClick}>
      <Box mr={2} position="relative">
        <AvatarName name={`${item?.name}`} />
        <div className={classes.statusContainer}>
          <div
            className={clsx(
              classes.status,
              item?.status === 'online' ? classes.online : classes.offline
            )}
          >
            <Icon
              className={clsx(
                classes.icon,
                item?.status === 'online'
                  ? classes.colorOnline
                  : classes.colorOffline
              )}
            >
              {item?.status === 'online' ? 'done' : 'remove'}
            </Icon>
          </div>
        </div>
      </Box>
      <ListItemText
        classes={{
          secondary: classes.textSecondary
        }}
        primary={`${item?.name}`}
        secondary={
          <Typography noWrap variant="body2" color="textSecondary">
            {item?.mood}
          </Typography>
        }
      />
      <Box className={classes.messageTime}>
        <Typography variant="caption" component="p" noWrap>
          Jan 11, 2022
        </Typography>
      </Box>
    </ListItemButton>
  );
};

export { ChatListItem };
