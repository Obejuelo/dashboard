import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  listItem: {
    borderBottom: 'solid 1px ' + theme.palette.divider,
    padding: '12px 16px',
    minHeight: 72
  },
  textSecondary: {
    textOverflow: 'ellipsis'
  },
  messageTime: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    height: 44,
    marginLeft: 4
  },
  statusContainer: {
    width: 15,
    height: 15,
    borderRadius: '50%',
    background: '#FFF',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    right: -2
  },
  status: {
    width: 12,
    height: 12,
    borderRadius: '50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  online: {
    border: 'solid 1.5px ' + theme.palette.success.main
  },
  offline: {
    border: 'solid 1.5px ' + theme.palette.error.main
  },
  icon: {
    fontSize: 9
  },
  colorOnline: {
    color: theme.palette.success.main
  },
  colorOffline: {
    color: theme.palette.error.main
  }
}));
