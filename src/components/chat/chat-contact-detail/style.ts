import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { grey } from '@mui/material/colors';

export const useStyles = makeStyles((theme: Theme) => ({
  header: {
    width: '100%',
    height: 150,
    background: theme.palette.primary.dark,
    padding: '1rem',
    position: 'relative',
    overflow: 'inherit'
  },
  name: {
    marginLeft: '0.3rem'
  },
  iconClose: {
    color: '#FFF'
  },
  status: {
    background: 'grey',
    width: 10,
    height: 10,
    borderRadius: 5,
    marginRight: 4
  },
  online: {
    background: theme.palette.success.main
  },
  offline: {
    background: grey[500]
  }
}));
