import React from 'react';
import { Box, Icon, IconButton, Typography } from '@mui/material';
import clsx from 'clsx';

import { useStyles } from './style';
import { AvatarName } from 'components/common/avatar/avatar-name';
import { ChatAppContactsType } from 'store/chat/chat.type';

interface ChatContactDetailProps {
  contact?: ChatAppContactsType;
  onClose: () => void;
}

const ChatContactDetail: React.FC<ChatContactDetailProps> = props => {
  const { contact, onClose } = props;
  const classes = useStyles();

  const getStatus = (status?: string) => (
    <div
      className={clsx(
        classes.status,
        status === 'online' && classes.online,
        status === 'offline' && classes.offline
      )}
    ></div>
  );

  return (
    <Box width={1}>
      <Box className={classes.header}>
        <Box position="absolute" top={8} right={8}>
          <IconButton onClick={onClose} size="small">
            <Icon className={classes.iconClose}>close</Icon>
          </IconButton>
        </Box>
        <Box
          position="absolute"
          bottom={-24}
          display="flex"
          alignItems="flex-end"
        >
          <AvatarName
            name={contact?.name as string}
            sx={{ width: 56, height: 56 }}
          />
          <Typography variant="h6" className={classes.name}>
            {contact?.name}
          </Typography>
        </Box>
      </Box>
      <Box px={2} mt={6}>
        <Typography variant="caption">status</Typography>
        <Box display="flex" alignItems="center">
          {getStatus(contact?.status)}
          <Typography>{contact?.status}</Typography>
        </Box>
      </Box>
      <Box px={2} mt={2}>
        <Typography variant="caption">Mood</Typography>
        <Typography>{contact?.mood}</Typography>
      </Box>
    </Box>
  );
};

export { ChatContactDetail };
