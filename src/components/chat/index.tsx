import React, { useState } from 'react';
import {
  AppBar,
  Box,
  Drawer,
  Icon,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography
} from '@mui/material';
import { useSelector } from 'react-redux';

import {
  useStyles,
  drawerTemporary,
  drawerPermanent,
  drawerUserTemporary
} from './style';
import { ChatSidebar } from './chat-sidebar';
import { AvatarName } from 'components/common/avatar/avatar-name';
import { ChatContactDetail } from './chat-contact-detail';
import { ChatContent } from './chat-content';
import { AppState } from 'store/reducer';

interface ChatProps {
  window?: () => Window;
}

const Chat: React.FC<ChatProps> = ({ window }) => {
  const classes = useStyles();
  const container =
    window !== undefined ? () => window().document.body : undefined;

  const [mobileOpen, setMobileOpen] = useState<boolean>(false);
  const [userDetailOpen, setUserDetailOpen] = useState<boolean>(false);
  const [moreMenuEl, setMoreMenuEl] = useState<null | HTMLElement>(null);

  const contacts = useSelector((state: AppState) => state.chat.contacts);
  const selectedContact = contacts.entities.find(
    item => item.id === contacts.selectedContactId
  );

  const handleMoreMenuClick = (event: React.MouseEvent<HTMLElement>) => {
    setMoreMenuEl(event.currentTarget);
  };

  const handleDetailToggle = () => setUserDetailOpen(!userDetailOpen);
  const handleDrawerToggle = () => setMobileOpen(!mobileOpen);
  const handleMoreMenuClose = () => setMoreMenuEl(null);
  const handleDrawerClose = () => setMobileOpen(false);

  return (
    <Box className={classes.container}>
      <Box>
        <Drawer
          anchor="left"
          open={mobileOpen}
          variant="temporary"
          container={container}
          onClose={handleDrawerToggle}
          className={classes.drawerMobile}
          classes={{ paper: classes.drawerPaper }}
          sx={drawerTemporary}
          ModalProps={{
            keepMounted: true,
            disablePortal: true,
            BackdropProps: {
              classes: {
                root: classes.drawerBackdrop
              }
            }
          }}
        >
          <ChatSidebar handleDrawerClose={handleDrawerClose} />
        </Drawer>
        <Drawer
          open
          variant="permanent"
          classes={{ paper: classes.drawerPaper }}
          sx={drawerPermanent}
        >
          <ChatSidebar handleDrawerClose={handleDrawerClose} />
        </Drawer>
      </Box>
      <main className={classes.contentWrapper}>
        <>
          <AppBar
            position="static"
            elevation={1}
            color="default"
            className={classes.chatAppBar}
          >
            <Toolbar className={classes.chatToolbar}>
              <Box display="flex" alignItems="center">
                <Box display={{ xs: 'block', lg: 'none' }}>
                  <IconButton
                    color="inherit"
                    aria-label="Open drawer"
                    onClick={handleDrawerToggle}
                  >
                    <Icon>menu</Icon>
                  </IconButton>
                </Box>

                {selectedContact && (
                  <>
                    <IconButton size="small" onClick={handleDetailToggle}>
                      <AvatarName
                        name={(selectedContact?.name as string) || ''}
                      />
                    </IconButton>
                    <Box ml={1}>
                      <Typography variant="h6">
                        {selectedContact?.name || ''}
                      </Typography>
                      <Typography variant="caption" component="p">
                        {selectedContact?.status}
                      </Typography>
                    </Box>
                  </>
                )}
              </Box>

              {selectedContact && (
                <Box>
                  <IconButton onClick={handleMoreMenuClick}>
                    <Icon>more_vert</Icon>
                  </IconButton>
                  <Menu
                    anchorEl={moreMenuEl}
                    open={Boolean(moreMenuEl)}
                    onClose={handleMoreMenuClose}
                  >
                    <MenuItem
                      onClick={() => {
                        handleMoreMenuClose();
                        handleDetailToggle();
                      }}
                    >
                      Contact info
                    </MenuItem>
                    <MenuItem onClick={handleMoreMenuClose}>
                      Mute notifications
                    </MenuItem>
                    <MenuItem onClick={handleMoreMenuClose}>
                      Delete chat
                    </MenuItem>
                  </Menu>
                </Box>
              )}
            </Toolbar>
          </AppBar>
          <div className={classes.content}>
            <ChatContent isChatSelected={selectedContact ? true : false} />
          </div>
        </>
      </main>
      <Drawer
        anchor="right"
        open={userDetailOpen}
        variant="temporary"
        container={container}
        onClose={handleDetailToggle}
        className={classes.drawerMobile}
        classes={{ paper: classes.drawerPaperRight }}
        sx={drawerUserTemporary}
        ModalProps={{
          keepMounted: true,
          disablePortal: true,
          BackdropProps: {
            classes: {
              root: classes.drawerBackdrop
            }
          }
        }}
      >
        <ChatContactDetail
          contact={selectedContact}
          onClose={handleDetailToggle}
        />
      </Drawer>
    </Box>
  );
};

export { Chat };
