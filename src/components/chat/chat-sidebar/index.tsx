import React, { useMemo, useState } from 'react';
import {
  AppBar,
  Box,
  Divider,
  Icon,
  IconButton,
  Input,
  Menu,
  MenuItem,
  Paper,
  Toolbar
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import { Scrollbar } from 'components/common/scrollbars/scrollbars';
import { AvatarName } from 'components/common/avatar/avatar-name';
import { ChatListItem } from '../chat-list-item';
import { AppState } from 'store/reducer';
import { useStyles } from './style';
import { getChat } from 'store/chat/chat.action';
import { filterArrayByString } from 'utils/search';

interface ChatSidebarProps {
  handleDrawerClose: () => void;
}

const ChatSidebar: React.FC<ChatSidebarProps> = props => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [searchText, setSearchText] = useState<string>('');
  const [moreMenuEl, setMoreMenuEl] = useState<null | HTMLElement>(null);

  const user = useSelector((state: AppState) => state.chat.user);
  const contacts = useSelector(
    (state: AppState) => state.chat.contacts.entities
  );

  const handleMoreMenuClick = (event: React.MouseEvent<HTMLElement>) => {
    setMoreMenuEl(event.currentTarget);
  };

  const handleMoreMenuClose = () => {
    setMoreMenuEl(null);
  };

  return (
    <Box height={1} display="flex" flexDirection="column">
      <AppBar position="static" color="default" elevation={1}>
        <Toolbar className={classes.toolbar}>
          <AvatarName name={user.name as string} />
          <Box>
            <IconButton onClick={handleMoreMenuClick}>
              <Icon>more_vert</Icon>
            </IconButton>
            <Menu
              anchorEl={moreMenuEl}
              open={Boolean(moreMenuEl)}
              onClose={handleMoreMenuClose}
            >
              <MenuItem onClick={handleMoreMenuClose}>New group</MenuItem>
              <MenuItem onClick={handleMoreMenuClose}>Config</MenuItem>
              <MenuItem onClick={handleMoreMenuClose}>Logout</MenuItem>
            </Menu>
          </Box>
        </Toolbar>
      </AppBar>
      <Box width={1} p={1}>
        <Paper className={classes.searchPaper} elevation={0}>
          <Icon className={classes.searchIcon} color="action">
            search
          </Icon>
          <Input
            placeholder="Search or start new chat"
            className="flex flex-1"
            disableUnderline
            fullWidth
            value={searchText}
            inputProps={{
              'aria-label': 'Search'
            }}
            onChange={e => setSearchText(e.target.value)}
          />
        </Paper>
      </Box>
      <Scrollbar className={classes.scrollbar}>
        <Divider />
        {useMemo(() => {
          const getFilteredArray = (arr: any[], searchText: string) => {
            if (searchText.length === 0) return arr;
            return filterArrayByString(arr, searchText);
          };

          const contactsArr = getFilteredArray([...contacts], searchText);

          return (
            <>
              {contactsArr.map(item => (
                <ChatListItem
                  onContactClick={() => {
                    dispatch(getChat(item.id));
                    props.handleDrawerClose();
                  }}
                  item={item}
                  key={item.id}
                />
              ))}
            </>
          );
        }, [contacts, user, searchText, dispatch])}
      </Scrollbar>
    </Box>
  );
};

export { ChatSidebar };
