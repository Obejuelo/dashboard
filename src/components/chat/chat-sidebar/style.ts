import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  avatar: {
    cursor: 'pointer'
  },
  searchIcon: {
    marginRight: 8
  },
  searchPaper: {
    display: 'flex',
    padding: 2,
    alignItems: 'center',
    width: '100%',
    paddingRight: 8,
    paddingLeft: 8,
    borderRadius: 20,
    border: `solid 1px ${theme.palette.divider}`
  },
  scrollbar: {
    overflowY: 'auto',
    flex: 1
  }
}));
