import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const drawerWidth = 400;

export const useStyles = makeStyles((theme: Theme) => ({
  container: {
    width: '100%',
    height: '100%',
    maxWidth: 1400,
    borderRadius: 8,
    background: '#EDEDED',
    display: 'flex',
    position: 'relative',
    overflow: 'hidden',
    boxShadow: theme.shadows[1]
  },
  drawerPaper: {
    width: drawerWidth,
    maxWidth: '100%',
    overflow: 'hidden',
    height: '100%',
    position: 'absolute',
    left: 0,
    zIndex: 2
  },
  drawerPaperRight: {
    width: drawerWidth,
    maxWidth: '100%',
    overflow: 'hidden',
    height: '100%',
    position: 'absolute',
    right: 0,
    zIndex: 3
  },
  drawerMobile: {
    position: 'absolute',
    zIndex: 20
  },
  drawerBackdrop: {
    position: 'absolute'
  },
  contentWrapper: {
    display: 'flex',
    flexDirection: 'column',
    flex: '1 1 100%',
    zIndex: 10,
    background: theme.palette.background.paper,
    marginLeft: 0,
    [theme.breakpoints.up('lg')]: {
      marginLeft: drawerWidth
    }
  },
  chatAppBar: {
    width: '100%'
  },
  chatToolbar: {
    padding: '0 1rem',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  content: {
    display: 'flex',
    flex: '1 1 100%',
    minHeight: 0
  }
}));

export const drawerTemporary = {
  display: { xs: 'block', lg: 'none' },
  '& .MuiDrawer-paper': {
    boxSizing: 'border-box',
    width: drawerWidth
  }
};

export const drawerPermanent = {
  display: { xs: 'none', lg: 'block' },
  '& .MuiDrawer-paper': {
    boxSizing: 'border-box',
    width: drawerWidth
  }
};

export const drawerUserTemporary = {
  display: { xs: 'block' },
  '& .MuiDrawer-paper': {
    boxSizing: 'border-box',
    width: drawerWidth
  }
};
