import React, { useEffect } from 'react';
import {
  Box,
  Icon,
  IconButton,
  Paper,
  TextField,
  Typography
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import clsx from 'clsx';

import { Scrollbar } from 'components/common/scrollbars/scrollbars';
import { AvatarName } from 'components/common/avatar/avatar-name';
import { ChatAppChatDialogType } from 'store/chat/chat.type';
import { AppState } from 'store/reducer';
import { useStyles } from './style';
import { sendMessage } from 'store/chat/chat.action';

interface ChatContentProps {
  isChatSelected?: boolean;
}

const ChatContent: React.FC<ChatContentProps> = props => {
  const { isChatSelected } = props;
  const classes = useStyles();
  const dispatch = useDispatch();

  const [scrollEl, setScrollEl] = React.useState<HTMLElement | null>(null);
  const [messageText, setMessageText] = React.useState('');

  const selectedContactId = useSelector(
    (state: AppState) => state.chat.contacts.selectedContactId
  );
  const contacts = useSelector(
    (state: AppState) => state.chat.contacts.entities
  );
  const chatApp = useSelector((state: AppState) => state.chat);
  const { chat, user } = chatApp;

  useEffect(() => {
    if (chat && scrollEl) {
      scrollEl.scrollTop = scrollEl.scrollHeight;
    }
  }, [chat, scrollEl]);

  const shouldShowContactAvatar = (item: ChatAppChatDialogType, i: number) => {
    return (
      item.who === selectedContactId &&
      ((chat.dialog[i + 1] && chat.dialog[i + 1].who !== selectedContactId) ||
        !chat.dialog[i + 1])
    );
  };

  const isFirstMessageOfGroup = (item: ChatAppChatDialogType, i: number) => {
    return (
      i === 0 || (chat.dialog[i - 1] && chat.dialog[i - 1].who !== item.who)
    );
  };

  const isLastMessageOfGroup = (item: ChatAppChatDialogType, i: number) => {
    return (
      i === chat.dialog.length - 1 ||
      (chat.dialog[i + 1] && chat.dialog[i + 1].who !== item.who)
    );
  };

  const onMessageSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    if (!messageText) return;

    await dispatch(sendMessage(messageText, chat.id, user.id));
    setMessageText('');
  };

  return (
    <Box display="flex" flex={1} zIndex={10} position="relative">
      <Scrollbar
        className={classes.scrollbar}
        containerRef={ref => setScrollEl(ref)}
      >
        {chat && chat.dialog.length > 0 ? (
          <Box className={classes.chatContent}>
            {chat.dialog.map((item, i) => {
              const contact =
                item.who === user.id
                  ? user
                  : contacts.find(_contact => _contact.id === item.who);

              return (
                <Box
                  key={item.time}
                  className={clsx(
                    classes.messageRow,
                    { me: item.who === user.id },
                    { contact: item.who !== user.id },
                    { 'first-of-group': isFirstMessageOfGroup(item, i) },
                    { 'last-of-group': isLastMessageOfGroup(item, i) }
                  )}
                >
                  {shouldShowContactAvatar(item, i) && (
                    <Box position="absolute" left={0} m={0}>
                      <AvatarName name={contact?.name as string} />
                    </Box>
                  )}
                  <Box
                    className="bubble"
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    position="relative"
                    py={1}
                    px={2}
                  >
                    <Typography variant="body2">{item.message}</Typography>
                  </Box>
                  <Typography
                    className={clsx(
                      classes.time,
                      'time',
                      item.who !== user.id
                        ? classes.timeLeft
                        : classes.timeRight
                    )}
                  >
                    {moment(item.time).format('MMMM Do YYYY, h:mm:ss a')}
                  </Typography>
                </Box>
              );
            })}
          </Box>
        ) : (
          <Box display="flex" flexDirection="column" flex={1}>
            <Box
              display="flex"
              flexDirection="column"
              flex={1}
              alignItems="center"
              justifyContent="center"
            >
              <Icon className={classes.iconChat} color="disabled">
                chat
              </Icon>
              <Typography color="textSecondary" variant="body2">
                Select a contact or start conversation
              </Typography>
            </Box>
          </Box>
        )}
      </Scrollbar>
      <form className={classes.sendFormContent} onSubmit={onMessageSubmit}>
        {isChatSelected && (
          <Paper className={classes.sendPaper} elevation={1}>
            <TextField
              autoFocus={false}
              className={classes.sendInput}
              variant="standard"
              value={messageText}
              onChange={e => setMessageText(e.target.value)}
              InputProps={{
                disableUnderline: true,
                classes: {
                  root: classes.sendInputProps,
                  input: ''
                },
                placeholder: 'Type your message'
              }}
            />
            <IconButton type="submit">
              <Icon color="action">send</Icon>
            </IconButton>
          </Paper>
        )}
      </form>
    </Box>
  );
};

export { ChatContent };
