import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  scrollbar: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    overflowY: 'auto',
    paddingBottom: 40
  },
  sendFormContent: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    padding: '1rem 0.5rem',
    background: theme.palette.background.paper
  },
  sendPaper: {
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    borderRadius: 24
  },
  sendInput: {
    flex: 1
  },
  sendInputProps: {
    display: 'flex',
    flexShrink: 0,
    margin: '0.5rem 0 0.5rem 1rem'
  },
  chatContent: {
    display: 'flex',
    flexDirection: 'column',
    padding: '1rem 1rem 40px 1rem'
  },
  iconChat: { fontSize: 128 },
  time: {
    position: 'absolute',
    overflow: 'hidden',
    fontSize: 11,
    marginBottom: -16,
    color: theme.palette.mode === 'dark' ? '#FFF' : '#000',
    bottom: 14,
    whiteSpace: 'nowrap'
  },
  messageRow: {
    display: 'flex',
    flexDirection: 'column',
    flexShrink: 0,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    position: 'relative',
    padding: '1rem',
    paddingLeft: 46,
    fontSize: 14,
    '&.contact': {
      '& .bubble': {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        '& .time': {
          marginLeft: 12
        }
      },
      '&.first-of-group': {
        '& .bubble': {
          borderTopLeftRadius: 20
        }
      },
      '&.last-of-group': {
        '& .bubble': {
          borderBottomLeftRadius: 20
        }
      }
    },
    '&.me': {
      paddingLeft: 40,

      '& .avatar': {
        order: 2,
        margin: '0 0 0 16px'
      },
      '& .bubble': {
        marginLeft: 'auto',
        backgroundColor:
          theme.palette.mode === 'light' ? theme.palette.grey[300] : '#1A2027',
        color:
          theme.palette.mode === 'light'
            ? theme.palette.getContrastText(theme.palette.grey[300])
            : '#FFF',
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        '& .time': {
          justifyContent: 'flex-end',
          right: 0,
          marginRight: 12
        }
      },
      '&.first-of-group': {
        '& .bubble': {
          borderTopRightRadius: 20
        }
      },

      '&.last-of-group': {
        '& .bubble': {
          borderBottomRightRadius: 20
        }
      }
    },
    '&.contact + .me, &.me + .contact': {
      paddingTop: 20,
      marginTop: 16
    },
    '&.first-of-group': {
      '& .bubble': {
        borderTopLeftRadius: 20,
        paddingTop: 13
      }
    },
    '&.last-of-group': {
      '& .bubble': {
        borderBottomLeftRadius: 20,
        paddingBottom: 13,
        '& .time': {
          display: 'flex'
        }
      }
    }
  },
  timeLeft: {
    left: 56
  },
  timeRight: {
    right: 0,
    textAlign: 'right',
    marginRight: '1rem'
  }
}));
