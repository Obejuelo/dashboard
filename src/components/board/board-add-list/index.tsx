import React, { useState } from 'react';
import {
  Box,
  Button,
  Card,
  ClickAwayListener,
  Icon,
  IconButton,
  InputAdornment,
  TextField
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useDispatch, useSelector } from 'react-redux';

import { addNewList } from 'store/board/board.actions';
import { AppState } from 'store/reducer';

const useStyles = makeStyles(() => ({
  icon: { fontSize: 36, marginRight: '0.5rem' },
  button: { height: '100%' },
  card: {
    width: 256,
    minWidth: 256,
    padding: '1rem',
    transition: 'all .3s',
    marginRight: 2
  }
}));

const BoardAddList: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [formOpen, setFormOpen] = useState(false);
  const [title, setTitle] = useState('');

  const board = useSelector((state: AppState) => state.board.board);

  const handleChangeForm = () => {
    setFormOpen(!formOpen);
    setTitle('');
  };

  function handleSubmit(ev: any) {
    ev.preventDefault();
    dispatch(addNewList(board.id, title));
    handleChangeForm();
  }

  return (
    <Card
      square
      className={classes.card}
      style={{ height: formOpen ? 128 : 84 }}
    >
      {formOpen ? (
        <ClickAwayListener onClickAway={handleChangeForm}>
          <form onSubmit={handleSubmit}>
            <TextField
              required
              fullWidth
              variant="outlined"
              label="List title"
              autoFocus
              name="title"
              value={title}
              onChange={e => setTitle(e.target.value)}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={handleChangeForm}>
                      <Icon className="text-18">close</Icon>
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
            <Box display="flex" justifyContent="flex-end" mt={1}>
              <Button
                variant="contained"
                color="secondary"
                type="submit"
                disabled={!title.length}
              >
                Add
              </Button>
            </Box>
          </form>
        </ClickAwayListener>
      ) : (
        <Button onClick={handleChangeForm} fullWidth className={classes.button}>
          <Icon className={classes.icon}>add_circle</Icon>
          Add a list
        </Button>
      )}
    </Card>
  );
};

export default BoardAddList;
