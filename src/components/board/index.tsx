import React from 'react';
import { Box } from '@mui/material';
import { useDispatch } from 'react-redux';
import {
  DragDropContext,
  Droppable,
  DroppableProvided,
  DropResult
} from 'react-beautiful-dnd';

import BoardList from './board-list';
import BoardAddList from './board-add-list';
import { reorderCard } from 'store/board/board.actions';
import { BoardListType } from 'store/board/board.types';

interface BoardProps {
  data: any;
}

const Board: React.FC<BoardProps> = ({ data }) => {
  const dispatch = useDispatch();

  function onDragEnd(result: DropResult) {
    const { source, destination } = result;

    // dropped nowhere
    if (!destination) return;

    // did not move anywhere - can bail early
    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    // reordering card
    if (result.type === 'card') {
      dispatch(reorderCard(result));
    }
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="list" type="list" direction="horizontal">
        {(provided: DroppableProvided) => (
          <Box ref={provided.innerRef} display="flex">
            {data.map((list: BoardListType) => (
              <BoardList key={list.id} list={list} />
            ))}
            {provided.placeholder}
            <BoardAddList />
          </Box>
        )}
      </Droppable>
    </DragDropContext>
  );
};

export default Board;
