import React, { useState } from 'react';
import {
  Box,
  Button,
  ClickAwayListener,
  Divider,
  Icon,
  IconButton,
  InputAdornment,
  TextField
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'store/reducer';
import { addNewCard } from 'store/board/board.actions';

interface BoardAddCardProps {
  listId: string;
  onCardAdded: () => void;
}

const BoardAddCard: React.FC<BoardAddCardProps> = props => {
  const { listId, onCardAdded } = props;
  const dispatch = useDispatch();
  const board = useSelector((state: AppState) => state.board.board);

  const [formOpen, setFormOpen] = useState(false);
  const [title, setTitle] = useState('');

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    await dispatch(addNewCard(board.id, listId, title));
    onCardAdded();
    setTitle('');
    setFormOpen(false);
  };

  return (
    <Box
      width={1}
      position="absolute"
      bottom={0}
      left={0}
      bgcolor="background.paper"
      padding={1}
    >
      <Divider />
      {formOpen ? (
        <ClickAwayListener onClickAway={() => setFormOpen(false)}>
          <form onSubmit={handleSubmit}>
            <TextField
              fullWidth
              required
              label="Card Title"
              autoFocus
              value={title}
              onChange={e => setTitle(e.target.value)}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={() => setFormOpen(false)}>
                      <Icon className="text-18">close</Icon>
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />

            <Box
              mt={1}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <Button
                fullWidth
                variant="contained"
                color="secondary"
                type="submit"
                disabled={title.length < 3}
              >
                Add
              </Button>
            </Box>
          </form>
        </ClickAwayListener>
      ) : (
        <Button onClick={() => setFormOpen(true)} fullWidth>
          <Box ml={1} component="span"></Box>
          <Icon>add</Icon>
          Add a card
        </Button>
      )}
    </Box>
  );
};

export default BoardAddCard;
