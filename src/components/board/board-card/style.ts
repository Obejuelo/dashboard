import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  card: {
    border: `solid 1px ${theme.palette.divider}`,
    marginBottom: '1rem',
    cursor: 'all-scroll',
    background:
      theme.palette.mode === 'dark'
        ? theme.palette.primary.dark
        : theme.palette.background.paper
  }
}));
