import React from 'react';
import { Box, Card, Typography } from '@mui/material';
import {
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot
} from 'react-beautiful-dnd';
import { useDispatch, useSelector } from 'react-redux';

import { useStyles } from './style';
import { AppState } from 'store/reducer';
import { openDialog } from 'store/configs/dialog/dialog.store.actions';
import BoardCardDialog from '../dialogs/card';

interface BoardCardProps {
  cardId: string;
  index: number;
}

const BoardCard: React.FC<BoardCardProps> = ({ cardId, index }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const cards = useSelector((state: AppState) => state.board.board.cards);
  const card = cards.find(item => item.id === cardId);

  const openCardDetail = () => {
    dispatch(
      openDialog({
        children: <BoardCardDialog card={card} />
      })
    );
  };

  return (
    <Draggable key={cardId} draggableId={cardId} index={index}>
      {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Card
            elevation={snapshot.isDragging ? 3 : 0}
            className={classes.card}
            onClick={openCardDetail}
          >
            <Box p={2}>
              <Typography noWrap variant="h6" align="center">
                {card?.name}
              </Typography>
              <Box mt={1}>
                <Typography color="textSecondary" align="justify">
                  {card?.description.slice(0, 64)}
                  {card?.description && card?.description.length >= 64
                    ? ' ...'
                    : ''}
                </Typography>
              </Box>
            </Box>
          </Card>
        </div>
      )}
    </Draggable>
  );
};

export default BoardCard;
