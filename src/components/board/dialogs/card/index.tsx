import React, { useState } from 'react';
import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  Grid,
  TextField,
  Typography
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import { closeDialog } from 'store/configs/dialog/dialog.store.actions';
import { useStyles } from './style';
import { updateCard } from 'store/board/board.actions';
import { AppState } from 'store/reducer';

interface BoardCardDialogProps {
  card: any;
}

const BoardCardDialog: React.FC<BoardCardDialogProps> = ({ card }) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const [title, setTitle] = useState(card.name || '');
  const [desc, setDesc] = useState(card.description || '');
  const boardId = useSelector((state: AppState) => state.board.board.id);

  const onUpdateCard = (e: any) => {
    e.preventDefault();

    const newCard: any = { ...card, name: title, description: desc };
    dispatch(updateCard(boardId, newCard));
  };

  return (
    <form onSubmit={onUpdateCard}>
      <DialogContent className={classes.dialog}>
        <Box width={1} mb={3}>
          <Typography variant="h4" align="center">
            Card detail
          </Typography>
        </Box>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              value={title}
              label="Title"
              className={classes.input}
              onChange={e => setTitle(e.target.value)}
              required
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              value={desc}
              label="Description"
              className={classes.input}
              multiline
              rows={8}
              onChange={e => setDesc(e.target.value)}
              required
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => dispatch(closeDialog())}>Cancel</Button>
        <Button type="submit" variant="contained" color="primary">
          Save
        </Button>
      </DialogActions>
    </form>
  );
};

export default BoardCardDialog;
