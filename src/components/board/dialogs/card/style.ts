import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export const useStyles = makeStyles((theme: Theme) => ({
  dialog: {
    width: '100%',
    padding: '0.5rem',
    [theme.breakpoints.up('md')]: {
      width: '35rem',
      padding: '1rem'
    }
  },
  input: {
    width: '100%'
  }
}));
