import React, { useRef } from 'react';
import {
  Box,
  Card,
  CardActions,
  Divider,
  Theme,
  Typography
} from '@mui/material';
import { Droppable, DroppableProvided } from 'react-beautiful-dnd';
import { makeStyles } from '@mui/styles';

import BoardCard from '../board-card';
import { BoardListType } from 'store/board/board.types';
import BoardAddCard from '../board-add-card';

interface BoardListProps {
  list: BoardListType;
}

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    marginRight: '1.5rem',
    marginBottom: '0.2rem',
    height: 'calc(100vh - 4.67rem)',
    width: '16rem',
    minWidth: '16rem',
    marginLeft: 2,
    position: 'relative',
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100vh - 5.2rem)'
    },
    [theme.breakpoints.up('md')]: {
      height: 'calc(100vh - 6.2rem)'
    }
  },
  listContainer: {
    height: 'calc(100vh - 200px)'
  }
}));

const BoardList: React.FC<BoardListProps> = ({ list }) => {
  const { idCards } = list;
  const classes = useStyles();
  const contentScrollEl = useRef<any>(null);

  const handleCardAdded = () => {
    if (contentScrollEl.current)
      contentScrollEl.current.scrollTop = contentScrollEl.current.scrollHeight;
  };

  return (
    <Card className={classes.paper} square>
      <Box p="1rem 1rem 0.5rem 1rem">
        <Typography variant="h5" noWrap align="center">
          {list.name}
        </Typography>
      </Box>
      <Divider />
      <div ref={contentScrollEl} className={classes.listContainer}>
        <Droppable droppableId={list.id} type="card" direction="vertical">
          {(provided: DroppableProvided) => (
            <Box ref={provided.innerRef} height="100%">
              <Box p={2} width="16rem" height="100%" overflow="auto">
                {idCards.map((cardId, index) => (
                  <BoardCard key={index} cardId={cardId} index={index} />
                ))}
              </Box>
              {provided.placeholder}
            </Box>
          )}
        </Droppable>
      </div>
      <CardActions>
        <BoardAddCard listId={list.id} onCardAdded={handleCardAdded} />
      </CardActions>
    </Card>
  );
};

export default BoardList;
