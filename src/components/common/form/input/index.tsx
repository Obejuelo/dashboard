import React, { FC, useState } from 'react';
import {
  IconButton,
  InputAdornment,
  TextField,
  OutlinedTextFieldProps
} from '@mui/material';
import { VisibilityOffOutlined, VisibilityOutlined } from '@mui/icons-material';
import { FormikProps } from 'formik';

interface IFormikInput extends OutlinedTextFieldProps {
  formik: FormikProps<any>;
  name: string;
}

type IInputProps = Partial<IFormikInput> &
  Pick<IFormikInput, 'formik' | 'name'> & {
    className?: string;
    maxLength?: number;
  };

export const Input: FC<IInputProps> = (props: IInputProps) => {
  const {
    formik,
    name,
    id,
    variant,
    type,
    InputProps,
    onChange,
    onBlur,
    helperText,
    disabled,
    ...otherProps
  } = props;

  const isPasswordType = type === 'password';
  const [isVisible, setIsVisible] = useState(false);
  const hasError = !!(formik.touched[name] && formik.errors[name]);

  return (
    <TextField
      fullWidth
      size="small"
      variant={variant || 'outlined'}
      id={id || name}
      name={name}
      type={isPasswordType ? (isVisible ? 'text' : 'password') : type || 'text'}
      value={formik.values[name]}
      onChange={onChange || formik.handleChange}
      onBlur={onBlur || formik.handleBlur}
      error={hasError}
      helperText={hasError ? formik.errors[name] : helperText || null}
      InputProps={
        isPasswordType && !disabled
          ? {
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    size="small"
                    onClick={(): void => setIsVisible(!isVisible)}
                  >
                    {isVisible ? (
                      <VisibilityOffOutlined color="primary" />
                    ) : (
                      <VisibilityOutlined color="primary" />
                    )}
                  </IconButton>
                </InputAdornment>
              )
            }
          : InputProps
      }
      inputProps={InputProps as any}
      disabled={disabled}
      {...otherProps}
    />
  );
};
