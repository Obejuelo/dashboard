import React from 'react';
import { CircularProgress, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { hexToRgba } from 'utils/style';

interface FormBodyProps {
  isLoading: boolean;
  spinnerSize?: number;
  backgroundColor?: string;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    position: 'relative'
  },
  spinnerWrapper: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: hexToRgba(theme.palette.background.paper, 0.5),
    zIndex: 9999
  }
}));

/**
 * A body wrapper that shows a loading spinner on submit
 * @example
 *
 * // Example with useFormik hook
 * <form onSubmit={formik.handleSubmit}>
 *  <FormBody isLoading={formik.isSubmitting}>
 *    // Your inputs here
 *  </FormBody>
 * </form>
 */

const FormBody: React.FC<FormBodyProps> = props => {
  const { isLoading, spinnerSize, backgroundColor } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div>{props.children}</div>
      {isLoading && (
        <div className={classes.spinnerWrapper} style={{ backgroundColor }}>
          <CircularProgress size={spinnerSize} />
        </div>
      )}
    </div>
  );
};

FormBody.defaultProps = {
  spinnerSize: 50
};

export default FormBody;
