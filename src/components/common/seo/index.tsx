import React from 'react';
import Head from 'next/head';

type DataProps = {
  description?: string;
  title: string;
};

export const SEO: React.FC<DataProps> = ({ description, title }) => {
  return (
    <>
      <Head>
        <title>Project | {title}</title>
        <meta name="twitter:description" content={description || ''} />
        <meta property="og:description" content={description || ''} />
        <meta name="twitter:creator" content={description || ''} />
        <meta name="description" content={description || ''} />
        <meta name="twitter:title" content={title || ''} />
        <meta property="og:title" content={title || ''} />
        <meta name="twitter:card" content="summary" />
        <meta property="og:type" content="website" />
      </Head>
    </>
  );
};
