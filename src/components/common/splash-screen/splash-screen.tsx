import React from 'react';
import { Box } from '@mui/system';
import { CircularProgress, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    background:
      theme.palette.mode === 'dark'
        ? theme.palette.primary.main
        : theme.palette.background.paper
  }
}));

const SplashScreen: React.FC = () => {
  const classes = useStyles();

  return (
    <Box
      width={1}
      height="100vh"
      display="flex"
      className={classes.root}
      justifyContent="center"
      alignItems="center"
    >
      <Box>
        <CircularProgress color="secondary" />
      </Box>
    </Box>
  );
};

export { SplashScreen };
