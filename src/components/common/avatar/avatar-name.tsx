import React from 'react';
import { Avatar, AvatarProps } from '@mui/material';
import clsx from 'clsx';

import { stringAvatar } from 'utils/string-avatar';
import { stringToColor } from 'utils/string-to-color';
import { makeStyles } from '@mui/styles';

interface AvatarNameProps extends AvatarProps {
  name: string;
}

const useStyles = makeStyles(() => ({
  uppercase: {
    textTransform: 'uppercase',
    color: '#FEFEFE'
  }
}));

const AvatarName: React.FC<AvatarNameProps> = props => {
  const classes = useStyles();
  const name = props.name || '';

  return (
    <Avatar
      {...props}
      className={clsx(props.className, classes.uppercase)}
      sx={{ ...props.sx, bgcolor: stringToColor(name as string) }}
    >
      {stringAvatar(name as string)}
    </Avatar>
  );
};

export { AvatarName };
