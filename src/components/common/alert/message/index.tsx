import React from 'react';
import {
  Box,
  Icon,
  IconButton,
  Snackbar,
  SnackbarContent,
  Typography
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';

import { hideMessage } from 'store/configs/message/message.store.action';
import { useStyles, variantIcon } from './style';
import { AppState } from 'store/reducer';

const AlertMessage: React.FC = () => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const state = useSelector((state: AppState) => state.message.state);
  const options = useSelector((state: AppState) => state.message.options);

  const handleClose = (): void => {
    dispatch(hideMessage());
  };

  return (
    <Snackbar
      {...options}
      open={state}
      onClose={handleClose}
      classes={{ root: classes.root }}
    >
      <SnackbarContent
        className={clsx(classes[options.variant])}
        message={
          <Box display="flex" alignItems="center">
            {variantIcon[options.variant] && (
              <Icon style={{ marginRight: 16 }} color="inherit">
                {variantIcon[options.variant]}
              </Icon>
            )}
            <Typography>{options.message}</Typography>
          </Box>
        }
        action={[
          <IconButton
            key="close"
            size="small"
            aria-label="Close"
            color="inherit"
            onClick={handleClose}
          >
            <Icon>close</Icon>
          </IconButton>
        ]}
      />
    </Snackbar>
  );
};

export { AlertMessage };
