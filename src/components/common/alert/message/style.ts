import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { green, amber, blue } from '@mui/material/colors';

export const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  success: {
    backgroundColor: green[600],
    color: '#FFFFFF'
  },
  error: {
    backgroundColor: theme.palette.error.main,
    color: theme.palette.getContrastText(theme.palette.error.main)
  },
  info: {
    backgroundColor: blue[600],
    color: '#FFFFFF'
  },
  warning: {
    backgroundColor: amber[600],
    color: '#FFFFFF'
  }
}));

export const variantIcon = {
  success: 'check_circle_outline',
  warning: 'warning_amber',
  error: 'error_outline',
  info: 'info'
};
