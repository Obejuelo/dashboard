import React from 'react';
import { Dialog } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import { closeDialog } from 'store/configs/dialog/dialog.store.actions';
import { AppState } from 'store/reducer';

const MainDialog: React.FC = () => {
  const dispatch = useDispatch();
  const dialog = useSelector((state: AppState) => state.dialog);

  return (
    <Dialog
      open={dialog.state}
      onClose={() => dispatch(closeDialog())}
      {...dialog.options}
    />
  );
};

export { MainDialog };
