import { green, amber, red, blue } from '@mui/material/colors';

export const lightPalette = {
  primary: {
    main: '#0B4F6C',
    light: '#117AA7',
    dark: '#07364a'
  },
  secondary: {
    main: '#B80C09',
    light: '#f56764',
    dark: '#8f0401'
  },
  success: {
    main: green[600]
  },
  warning: {
    main: amber[600]
  },
  error: {
    main: red.A400
  },
  info: {
    main: blue[600]
  },
  background: {
    default: '#fbfbff',
    paper: '#FFFFFF'
  },
  divider: 'rgba(0, 0, 0, 0.12)'
};

export const darkPalette = {
  primary: {
    main: '#0B4F6C',
    light: '#117AA7',
    dark: '#07364a'
  },
  secondary: {
    main: '#B80C09',
    light: '#f56764',
    dark: '#8f0401'
  },
  success: {
    main: green[600]
  },
  warning: {
    main: amber[600]
  },
  error: {
    main: red.A400
  },
  info: {
    main: blue[600]
  },
  background: {
    default: '#0b2b38',
    paper: '#000000'
  },
  divider: 'rgba(255, 255, 255, 0.12)'
};
