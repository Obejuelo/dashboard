import React, { ReactNode, useMemo } from 'react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { StyledEngineProvider } from '@mui/material/styles';

import darkTheme from './dark';
import lightTheme from './light';
import { useSelector } from 'react-redux';
import { AppState } from 'store/reducer';

interface ThemeLayoutProps {
  children: ReactNode;
}

const ThemeLayout: React.FC<ThemeLayoutProps> = (props: ThemeLayoutProps) => {
  const user = useSelector((state: AppState) => state.user.data);
  const theme: any = useMemo(
    () => (user.theme === 'light' ? lightTheme : darkTheme),
    [user.theme]
  );

  // Override MUI components
  theme.components = {
    ...theme.components
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {/* To not use !important in styles use <StyledEngineProvider> */}
      <StyledEngineProvider injectFirst>{props.children}</StyledEngineProvider>
    </ThemeProvider>
  );
};

export default React.memo(ThemeLayout);
