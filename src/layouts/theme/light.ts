import { createTheme, ThemeOptions } from '@mui/material/styles';

import { lightPalette } from './config/palette';
import baseConfig from './config';

const lightTheme: ThemeOptions = createTheme({
  ...baseConfig,
  palette: {
    mode: 'light',
    ...lightPalette
  }
});

export default lightTheme;
