import React from 'react';
import { Box } from '@mui/system';

import { AlertMessage } from 'components/common/alert';

type AuthContainerProps = {
  children?: React.ReactElement;
};

const AuthContainer: React.FC<AuthContainerProps> = props => {
  return (
    <Box
      width={1}
      height="100vh"
      overflow="hidden"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      {props.children}
      <AlertMessage />
    </Box>
  );
};

export { AuthContainer };
