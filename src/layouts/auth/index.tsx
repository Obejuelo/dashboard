import React from 'react';

import { AuthContainer } from './container';
import { useRouteGuard } from 'hooks';
import { SplashScreen } from 'components/common/splash-screen/splash-screen';

type AuthLayoutProps = {
  children?: React.ReactElement;
};

const AuthLayout: React.FC<AuthLayoutProps> = props => {
  const isValidSession = useRouteGuard('PUBLIC');

  return isValidSession === false ? (
    <AuthContainer>{props.children}</AuthContainer>
  ) : (
    <SplashScreen />
  );
};

export { AuthLayout };
