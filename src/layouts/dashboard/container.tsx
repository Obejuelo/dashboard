import React from 'react';
import { Drawer } from '@mui/material';

import { drawerPermanent, drawerTemporary, sizes, useStyles } from './style';
import { Scrollbar } from '../../components/common/scrollbars/scrollbars';
import ToolbarLayout from './components/toolbar/toolbar-layout';
import NavBarLayout from './components/navbar/navbar-layout';
import { AlertMessage } from 'components/common/alert';
import { MainDialog } from 'components/common/dialog';

interface DashboardWrapperProps {
  window?: () => Window;
  children?: React.ReactElement;
}

const DashboardWrapper: React.FC<DashboardWrapperProps> = props => {
  const { window } = props;
  const classes = useStyles(sizes);
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const container =
    window !== undefined ? () => window().document.body : undefined;

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className={classes.root}>
      <ToolbarLayout
        handleDrawerToggle={handleDrawerToggle}
        drawerWidth={sizes.drawerWidth}
      />
      <nav className={classes.drawer}>
        <Drawer
          anchor="left"
          open={mobileOpen}
          variant="temporary"
          container={container}
          onClose={handleDrawerToggle}
          ModalProps={{ keepMounted: true }}
          classes={{ paper: classes.drawerPaper }}
          sx={drawerTemporary}
        >
          <NavBarLayout />
        </Drawer>
        <Drawer
          open
          variant="permanent"
          classes={{ paper: classes.drawerPaper }}
          sx={drawerPermanent}
        >
          <NavBarLayout />
        </Drawer>
      </nav>
      <main className={classes.content}>
        <Scrollbar>{props.children}</Scrollbar>
      </main>

      <AlertMessage />
      <MainDialog />
    </div>
  );
};

export default DashboardWrapper;
