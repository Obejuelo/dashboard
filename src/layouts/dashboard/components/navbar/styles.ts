import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  listIcon: { marginRight: '0.5rem', color: '#FFF' },
  active: { background: theme.palette.primary.light },
  textItem: { color: '#FFF' },
  listContainer: { paddingTop: 0 },
  root: {
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    height: '100vh',
    maxHeight: '100vh',
    background: theme.palette.primary.main
  },
  appBar: {
    display: 'flex',
    alignItems: 'center',
    flexShrink: 0,
    height: '4rem',
    background: theme.palette.primary.main
  },
  listItem: {
    color: theme.palette.background.default,
    padding: '1rem',
    '&:hover': {
      background: theme.palette.primary.light
    }
  }
}));
