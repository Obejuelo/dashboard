import React from 'react';
import { AppBar, Box, Typography, List, ListItem, Icon } from '@mui/material';
import { useRouter } from 'next/router';
import clsx from 'clsx';

import { Scrollbar } from '../../../../components/common/scrollbars/scrollbars';
import { useStyles } from './styles';
import { appConfig } from 'core/config';

const NavBarLayout: React.FC = () => {
  const router = useRouter();
  const classes = useStyles();
  const path = router.pathname;

  return (
    <div className={clsx(classes.root)}>
      <AppBar
        color="primary"
        position="static"
        elevation={0}
        className={classes.appBar}
      >
        <Box display="flex" px="1rem" alignItems="center" flex={1}>
          <Typography>LOGO</Typography>
        </Box>
      </AppBar>
      <Scrollbar>
        <List className={classes.listContainer}>
          {appConfig.sidebarMenu.map(item => (
            <ListItem
              button
              key={item.id}
              onClick={() => router.push(item.path)}
              className={clsx(
                classes.listItem,
                path === item.path && classes.active
              )}
            >
              <Icon className={classes.listIcon}>{item.icon}</Icon>
              <Typography noWrap className={classes.textItem}>
                {item.name}
              </Typography>
            </ListItem>
          ))}
        </List>
      </Scrollbar>
    </div>
  );
};

export default NavBarLayout;
