import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { ToolbarLayoutProps } from './toolbar-layout';

export const useStyles = makeStyles((theme: Theme) => ({
  icon: { color: '#FFF' },
  appBar: {
    [theme.breakpoints.up('md')]: {
      width: (props: ToolbarLayoutProps) =>
        `calc(100% - ${props.drawerWidth}px)`,
      marginLeft: props => props.drawerWidth
    }
  },
  toolbar: {
    padding: '0 1rem',
    display: 'flex',
    justifyContent: 'space-between'
  },
  menuButton: {
    display: 'inline-flex',
    color: '#FEFEFE',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  }
}));
