import React from 'react';
import { Box, Tooltip } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import { AppState } from 'store/reducer';
import { setUserData } from 'store/user/user.action';
import { CustomSwitch } from './style';

const SwitchDarkMode: React.FC = () => {
  const dispatch = useDispatch();
  const themeMode = useSelector((state: AppState) => state.user.data.theme);
  const [checked, setChecked] = React.useState(
    themeMode === 'dark' ? true : false
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const theme = event.target.checked ? 'dark' : 'light';
    setChecked(event.target.checked);
    dispatch(setUserData({ theme }));
  };

  return (
    <Box display="flex" alignItems="center" mr={1}>
      <Tooltip
        title={`Toggle ${themeMode === 'dark' ? 'light' : 'dark'} theme`}
      >
        <CustomSwitch
          size="small"
          checked={checked}
          onChange={handleChange}
          inputProps={{ 'aria-label': 'controlled' }}
        />
      </Tooltip>
    </Box>
  );
};

export { SwitchDarkMode };
