import React from 'react';
import {
  AppBar,
  Box,
  Icon,
  IconButton,
  Toolbar,
  Typography
} from '@mui/material';

import { useStyles } from './styles';
import { AccountMenu } from './account-menu';
import { SwitchDarkMode } from './switch-dark-mode';

export interface ToolbarLayoutProps {
  handleDrawerToggle: () => void;
  drawerWidth: number;
}

const ToolbarLayout: React.FC<ToolbarLayoutProps> = props => {
  const classes = useStyles(props);

  return (
    <AppBar
      position="fixed"
      color="secondary"
      elevation={0}
      className={classes.appBar}
    >
      <Toolbar className={classes.toolbar}>
        <Box display="flex" alignItems="center">
          <IconButton
            onClick={props.handleDrawerToggle}
            className={classes.menuButton}
            edge="start"
            id="burger-menu"
          >
            <Icon>menu</Icon>
          </IconButton>
          <Typography variant="h5">Enterprise UI</Typography>
        </Box>

        <Box display="flex">
          <SwitchDarkMode />
          <AccountMenu />
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default ToolbarLayout;
