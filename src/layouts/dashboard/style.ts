import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface SizesProps {
  drawerWidth: number;
  topBarHeight: number;
  topBarHeightXs: number;
}

export const sizes: SizesProps = {
  drawerWidth: 250,
  topBarHeight: 64,
  topBarHeightXs: 56
};

export const drawerTemporary = {
  display: { xs: 'block', md: 'none' },
  '& .MuiDrawer-paper': {
    boxSizing: 'border-box',
    width: sizes.drawerWidth
  }
};

export const drawerPermanent = {
  display: { xs: 'none', md: 'block' },
  '& .MuiDrawer-paper': {
    boxSizing: 'border-box',
    width: sizes.drawerWidth
  }
};

export const useStyles = makeStyles((theme: Theme) => ({
  root: { display: 'flex' },
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: sizes.drawerWidth,
      flexShrink: 0
    }
  },
  drawerPaper: {
    width: sizes.drawerWidth,
    backgroundColor: theme.palette.primary.dark
  },
  content: {
    flexGrow: 1,
    overflow: 'auto',
    position: 'relative',
    background: theme.palette.background.default,
    height: `calc(100vh - ${sizes.topBarHeightXs}px)`,
    maxHeight: `calc(100vh - ${sizes.topBarHeightXs}px)`,
    marginTop: sizes.topBarHeightXs,
    [theme.breakpoints.up('sm')]: {
      marginTop: sizes.topBarHeight,
      height: `calc(100vh - ${sizes.topBarHeight}px)`,
      maxHeight: `calc(100vh - ${sizes.topBarHeight}px)`
    }
  }
}));
