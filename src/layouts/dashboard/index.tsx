import React from 'react';

import { useRouteGuard } from 'hooks';
import DashboardWrapper from './container';
import { SplashScreen } from 'components/common/splash-screen/splash-screen';

type DashboardLayoutProps = {
  children?: React.ReactElement;
};

/**
 * Side nav and top bar wrapper. Use it in every page that requires the Dashboard.
 * @example
 *
 * <DashboardLayout>
 *  // page content here
 * </DashboardLayout>
 */
const DashboardLayout: React.FC<DashboardLayoutProps> = props => {
  const isValidSession = useRouteGuard('PRIVATE');

  return isValidSession ? (
    <DashboardWrapper>{props.children}</DashboardWrapper>
  ) : (
    <SplashScreen />
  );
};

export { DashboardLayout };
